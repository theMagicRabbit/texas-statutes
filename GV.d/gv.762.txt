       
                        
    GOVERNMENT CODE TITLE 7. INTERGOVERNMENTAL RELATIONS CHAPTER 762.
    COMMISSION ON UNIFORM STATE LAWS Sec. 762.001.  DEFINITIONS.  In this
    chapter:(1)  "Commission" means the Commission on Uniform State Laws.(2)
    "National conference" means the National Conference of Commissioners on
    Uniform State Laws.  Added by Acts 1991, 72nd Leg., ch. 38, Sec. 1, eff.
    Sept. 1, 1991.

    Sec. 762.002.  DUTIES OF COMMISSION.  (a)  The commission shall:(1)
    promote the uniform judicial interpretation of all uniform laws;  and(2)
    before January 1 of each odd-numbered year, submit a biennial report to
    the legislature that contains an account of the commission's transactions
    and its advice and recommendations for legislation.(b)  The commission may
    supplement the report.  Added by Acts 1991, 72nd Leg., ch. 38, Sec. 1,
    eff. Sept. 1, 1991.

    Sec. 762.003.  COMPOSITION OF COMMISSION;  TERMS.  (a)  The commission is
    composed of:(1)  nine members appointed by the governor;(2)  the executive
    director of the Texas Legislative Council or a person designated by the
    executive director;  and(3)  in addition to the persons described by
    Subdivisions (1) and (2), residents of this state who have long service in
    the cause of uniformity in state legislation as shown by:(A)  at least 20
    years of service representing the state as an associate member of the
    national conference;(B)  election as a life member of the national
    conference;  or(C)  at least 15 years of service as a member of the
    commission and at least five years of combined service as a judge or
    justice of a trial or appellate court of this state.(b)  Appointments to
    the commission shall be made without regard to the race, creed, sex,
    religion, or national origin of the appointees.(c)  Appointed members
    serve staggered six-year terms, with the terms of three members expiring
    September 30 of each even-numbered year.  Added by Acts 1991, 72nd Leg.,
    ch. 38, Sec. 1, eff. Sept. 1, 1991.  Amended by Acts 1999, 76th Leg., ch.
    170, Sec. 1, eff. May 21, 1999;  Acts 2001, 77th Leg., ch. 30, Sec. 1,
    eff. July 1, 2001.

    Sec. 762.004.  ELIGIBILITY FOR APPOINTMENT;  LOBBYIST RESTRICTION.  (a)
    To be eligible for appointment to the commission, a person must be an
    attorney licensed to practice law.(b)  At least one of the commissioners,
    at the time of that commissioner's appointment, must be a state judge.(c)
    At least one of the commissioners, at the time of that commissioner's
    appointment, must be a legal educator.(d)  A person required to register
    as a lobbyist under Chapter 305 because of the person's activities for
    compensation in or on behalf of a profession related to the operation of
    the commission may not serve as a commissioner or act as general counsel
    to the commission.  Added by Acts 1991, 72nd Leg., ch. 38, Sec. 1, eff.
    Sept. 1, 1991.

    Sec. 762.005.  DUTIES OF COMMISSIONERS.  Each commissioner shall:(1)
    promote uniformity in state laws in subject areas in which uniformity is
    desirable and practicable;  and(2)  attend national conference meetings.
    Added by Acts 1991, 72nd Leg., ch. 38, Sec. 1, eff. Sept. 1, 1991.

    Sec. 762.006.  VACANCY;  EXPIRATION OF TERM.  (a)  The office of an
    appointed commissioner becomes vacant on the death, resignation, failure
    or refusal to serve, or removal of the commissioner.(b)  The governor
    shall fill a vacancy by appointing a person to the commission for the
    unexpired term of the commissioner vacating the office.(c)  On the vacancy
    or expiration of the term of office of an appointed commissioner, the
    governor shall appoint a state judge or legal educator if the appointment
    is required by Section 762.004(b) or (c).  Added by Acts 1991, 72nd Leg.,
    ch. 38, Sec. 1, eff. Sept. 1, 1991.

    Sec. 762.008.  GROUNDS FOR REMOVAL.  (a)  It is a ground for removal from
    the commission if a member:(1)  did not have, at the time of appointment
    or election, the qualifications required by Section 762.004;(2)  does not
    maintain the qualifications required by Section 762.004;(3)  is prohibited
    from serving as a commissioner under Section 762.004(d);  or(4)  is
    ineligible to participate in activities of the national conference.(b)
    The validity of an action of the commission is not affected because it is
    taken when a member is subject to removal.  Added by Acts 1991, 72nd Leg.,
    ch. 38, Sec. 1, eff. Sept. 1, 1991.

    Sec. 762.009.  MEETING AND ELECTION OF OFFICERS.  (a)  The commission
    shall meet at least once every two years.(b)  The commissioners shall
    elect a chairman and secretary, who shall each hold office for a term of
    two years.  Added by Acts 1991, 72nd Leg., ch. 38, Sec. 1, eff. Sept. 1,
    1991.

    Sec. 762.010.  COMPENSATION.  A commissioner serves without compensation
    but is entitled to be reimbursed for reasonable expenses incurred in the
    performance of the commissioner's duties.  Added by Acts 1991, 72nd Leg.,
    ch. 38, Sec. 1, eff. Sept. 1, 1991.

    Sec. 762.011.  SUPPORT SERVICES.  The Texas Legislative Council shall
    provide accounting, clerical, and other support services necessary for the
    commission to carry out its duties.  Added by Acts 1991, 72nd Leg., ch.
    38, Sec. 1, eff. Sept. 1, 1991.


                    
