     
                        
    GOVERNMENT CODE TITLE 9. PUBLIC SECURITIES SUBTITLE I. SPECIFIC AUTHORITY
    FOR COUNTIES TO ISSUE SECURITIES CHAPTER 1476. CERTIFICATES OF
    INDEBTEDNESS IN COUNTIES WITH POPULATION OF MORE THAN TWO MILLION Sec.
    1476.001.  APPLICABILITY OF CHAPTER.  (a)  This chapter applies only to a
    county with a population of more than two million.(b)  If certificates of
    indebtedness were not issued under this chapter by January 1, 1980, this
    chapter has no effect.  Added by Acts 1999, 76th Leg., ch. 227, Sec. 1,
    eff. Sept. 1, 1999.Amended by: Acts 2011, 82nd Leg., R.S., Ch. 1163 (H.B.
    2702), Sec. 29, eff. September 1, 2011.

    Sec. 1476.002.  AUTHORITY TO ISSUE CERTIFICATES OF INDEBTEDNESS FOR
    CERTAIN PURPOSES.  A county may issue certificates of indebtedness:(1)  in
    an amount not to exceed $2 million to construct, enlarge, furnish, equip,
    or repair a county building or other permanent improvement;  or(2)  in an
    amount not to exceed $3.5 million to:(A)  purchase right-of-way in
    participation with the Texas Department of Transportation in connection
    with a designated state highway;  or(B)  construct a curb, gutter, or
    drainage facility for a designated state highway.  Added by Acts 1999,
    76th Leg., ch. 227, Sec. 1, eff. Sept. 1, 1999.

    Sec. 1476.003.  AUTHORIZATION OF CERTIFICATES OF INDEBTEDNESS BY
    COMMISSIONERS COURT.  Certificates of indebtedness issued under this
    chapter must be authorized by order of the commissioners court of the
    county.  Added by Acts 1999, 76th Leg., ch. 227, Sec. 1, eff. Sept. 1,
    1999.

    Sec. 1476.004.  EXECUTION;  REGISTRATION BY COUNTY TREASURER.  A
    certificate of indebtedness issued under this chapter must be:(1)  signed
    by the county judge;(2)  attested by the county clerk;  and(3)  registered
    by the county treasurer.  Added by Acts 1999, 76th Leg., ch. 227, Sec. 1,
    eff. Sept. 1, 1999.

    Sec. 1476.005.  CASH SALE.  A county shall sell certificates of
    indebtedness issued under this chapter for cash.  Added by Acts 1999, 76th
    Leg., ch. 227, Sec. 1, eff. Sept. 1, 1999.

    Sec. 1476.006.  MATURITY.  A certificate of indebtedness issued under this
    chapter must mature not later than 35 years after its date.  Added by Acts
    1999, 76th Leg., ch. 227, Sec. 1, eff. Sept. 1, 1999.


                    
