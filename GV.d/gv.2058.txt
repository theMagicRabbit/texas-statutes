     
                        
    GOVERNMENT CODE TITLE 10. GENERAL GOVERNMENT SUBTITLE B. INFORMATION AND
    PLANNING CHAPTER 2058. RECOGNITION OF FEDERAL CENSUS Sec. 2058.001.
    GOVERNMENTAL RECOGNITION OF AND ACTION ON FEDERAL CENSUS.  (a)  A
    governmental entity may not recognize or act on a report or publication,
    in any form, of a federal decennial census, in whole or in part, before
    September 1 of the year after the calendar year during which the census
    was taken.(b)  A governmental entity shall recognize and act on a
    published report or count relating to a federal decennial census and
    released by the director of the Bureau of the Census of the United States
    Department of Commerce:(1)  on September 1 of the year after the calendar
    year during which the census was taken if the report or count is published
    on or before that date;  or(2)  on the date of its publication if the
    report or count is published after September 1 of the year after the
    calendar year during which the census was taken.(c)  In this section,
    "governmental entity" means the state or an agency or political
    subdivision of the state.  Added by Acts 1993, 73rd Leg., ch. 268, Sec. 1,
    eff. Sept. 1, 1993.

    Sec. 2058.002.  EXCEPTIONS.  (a)  The legislature or the Legislative
    Redistricting Board under Article III, Section 28, of the Texas
    Constitution may officially recognize or act on a federal decennial census
    before September 1 of the year after the calendar year during which the
    census was taken.(b)  A political subdivision governed by a body elected
    from single-member districts may recognize and act on tabulations of
    population of a federal decennial census, for redistricting purposes, on
    or after the date the governor receives a report of the basic tabulations
    of population from the secretary of commerce under 13 U.S.C. Section
    141(c).  This subsection does not apply to a political subdivision that
    was not subject to a statute requiring certain political subdivisions,
    classified by population, to elect their governing bodies from
    single-member districts under the preceding federal census.  Added by Acts
    1993, 73rd Leg., ch. 268, Sec. 1, eff. Sept. 1, 1993.


                    
