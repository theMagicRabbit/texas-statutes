      
                        
    GOVERNMENT CODE TITLE 2. JUDICIAL BRANCH SUBTITLE F. COURT ADMINISTRATION
    CHAPTER 78.  CAPITAL AND FORENSIC WRITS COMMITTEE AND OFFICE OF CAPITAL
    AND FORENSIC WRITS SUBCHAPTER A.  CAPITAL AND FORENSIC WRITS COMMITTEE
    Sec. 78.001.  DEFINITIONS.  In this subchapter:(1)  "Committee" means the
    capital and forensic writs committee established under this subchapter.(2)
    "Office of capital and forensic writs" means the office of capital and
    forensic writs established under Subchapter B.  Added by Acts 2009, 81st
    Leg., R.S., Ch. 781 (S.B. 1091), Sec. 1, eff. September 1, 2009.Amended
    by: Acts 2015, 84th Leg., R.S., Ch. 1215 (S.B. 1743), Sec. 12, eff.
    September 1, 2015.

    Sec. 78.002.  ESTABLISHMENT OF COMMITTEE; DUTIES.  (a)  The capital and
    forensic writs committee is established.(b)  The committee shall recommend
    to the court of criminal appeals as provided by Section 78.004 a director
    for the office of capital and forensic writs when a vacancy exists for the
    position of director.  Added by Acts 2009, 81st Leg., R.S., Ch. 781 (S.B.
    1091), Sec. 1, eff. September 1, 2009.Amended by: Acts 2015, 84th Leg.,
    R.S., Ch. 1215 (S.B. 1743), Sec. 13, eff. September 1, 2015.

    Sec. 78.003.  APPOINTMENT AND COMPOSITION OF COMMITTEE.  (a)  The
    committee is composed of the following five members who are appointed by
    the president of the State Bar of Texas, with ratification by the
    executive committee of the State Bar of Texas:(1)  three attorneys who are
    members of the State Bar of Texas and who are not employed as prosecutors
    or law enforcement officials, all of whom must have criminal defense
    experience with death penalty proceedings in this state; and(2)  two state
    district judges, one of whom serves as presiding judge of an
    administrative judicial region.(b)  The committee shall elect one member
    of the committee to serve as the presiding officer of the committee.(c)
    The committee members serve at the pleasure of the president of the State
    Bar of Texas, and the committee meets at the call of the presiding officer
    of the committee.  Added by Acts 2009, 81st Leg., R.S., Ch. 781 (S.B.
    1091), Sec. 1, eff. September 1, 2009.

    Sec. 78.004.  RECOMMENDATION AND APPOINTMENT OF DIRECTOR OF OFFICE OF
    CAPITAL AND FORENSIC WRITS.  (a)  The committee shall submit to the court
    of criminal appeals, in order of the committee's preference, a list of the
    names of not more than five persons the committee recommends that the
    court consider in appointing the director of the office of capital and
    forensic writs when a vacancy exists for the position of director.  If the
    committee finds that three or more persons under the committee's
    consideration are qualified to serve as the director of the office of
    capital and forensic writs, the committee must include at least three
    names in the list submitted under this subsection.(b)  Each person
    recommended to the court of criminal appeals by the committee under
    Subsection (a):(1)  must exhibit proficiency and commitment to providing
    quality representation to defendants in death penalty cases, as described
    by the Guidelines and Standards for Texas Capital Counsel, as published by
    the State Bar of Texas; and(2)  may not have been found by a state or
    federal court to have rendered ineffective assistance of counsel during
    the trial or appeal of a criminal case.(c)  When a vacancy for the
    position exists, the court of criminal appeals shall appoint from the list
    of persons submitted to the court under Subsection (a) the director of the
    office of capital and forensic writs.  Added by Acts 2009, 81st Leg.,
    R.S., Ch. 781 (S.B. 1091), Sec. 1, eff. September 1, 2009.Amended by: Acts
    2015, 84th Leg., R.S., Ch. 1215 (S.B. 1743), Sec. 14, eff. September 1,
    2015.Acts 2015, 84th Leg., R.S., Ch. 1215 (S.B. 1743), Sec. 15, eff.
    September 1, 2015.

    SUBCHAPTER B.  OFFICE OF CAPITAL AND FORENSIC WRITS Sec. 78.051.
    DEFINITIONS.  In this subchapter:(1)  "Committee" means the capital and
    forensic writs committee established under Subchapter A.(2)  "Office"
    means the office of capital and forensic writs established under this
    subchapter.  Added by Acts 2009, 81st Leg., R.S., Ch. 781 (S.B. 1091),
    Sec. 1, eff. September 1, 2009.Amended by: Acts 2015, 84th Leg., R.S., Ch.
    1215 (S.B. 1743), Sec. 17, eff. September 1, 2015.

    Sec. 78.052.  ESTABLISHMENT; FUNDING.  (a)  The office of capital and
    forensic writs is established and operates under the direction and
    supervision of the director of the office.(b)  The office shall receive
    funds for personnel costs and expenses:(1)  as specified in the General
    Appropriations Act; and(2)  from the fair defense account under Section
    79.031, in an amount sufficient to cover personnel costs and expenses not
    covered by appropriations described by Subdivision (1).  Added by Acts
    2009, 81st Leg., R.S., Ch. 781 (S.B. 1091), Sec. 1, eff. September 1,
    2009.Amended by: Acts 2011, 82nd Leg., R.S., Ch. 984 (H.B. 1754), Sec. 3,
    eff. September 1, 2011.Acts 2015, 84th Leg., R.S., Ch. 1215 (S.B. 1743),
    Sec. 18, eff. September 1, 2015.

    Sec. 78.053.  DIRECTOR; STAFF.  (a)  The court of criminal appeals shall
    appoint a director to direct and supervise the operation of the office.
    The director serves a four-year term and continues to serve until a
    successor has been appointed and qualified.  The court of criminal appeals
    may remove the director only for good cause.  The director may be
    reappointed for a second or subsequent term.(b)  The director shall employ
    attorneys and employ or retain licensed investigators, experts, and other
    personnel necessary to perform the duties of the office.  To be employed
    by the director, an attorney may not have been found by a state or federal
    court to have rendered ineffective assistance of counsel during the trial
    or appeal of a criminal case.(c)  The director and any attorney employed
    by the office may not:(1)  engage in the private practice of criminal law;
    or(2)  accept anything of value not authorized by law for services
    rendered under this subchapter.  Added by Acts 2009, 81st Leg., R.S., Ch.
    781 (S.B. 1091), Sec. 1, eff. September 1, 2009.Amended by: Acts 2015,
    84th Leg., R.S., Ch. 1215 (S.B. 1743), Sec. 19, eff. September 1, 2015.

    Sec. 78.054.  POWERS AND DUTIES.  (a)  The office may not accept an
    appointment under Article 11.071, Code of Criminal Procedure, if:(1)  a
    conflict of interest exists;(2)  the office has insufficient resources to
    provide adequate representation for the defendant;(3)  the office is
    incapable of providing representation for the defendant in accordance with
    the rules of professional conduct; or(4)  other good cause is shown for
    not accepting the appointment.(b)  The office may not represent a
    defendant in a federal habeas review.  The office may not represent a
    defendant in an action or proceeding in state court other than an action
    or proceeding that:(1)  is conducted under Article 11.071, Code of
    Criminal Procedure;(2)  is collateral to the preparation of an application
    under Article 11.071, Code of Criminal Procedure;(3)  concerns any other
    post-conviction matter in a death penalty case other than a direct appeal,
    including an action or proceeding under Article 46.05 or Chapter 64, Code
    of Criminal Procedure; or(4)  is conducted under Article 11.073, Code of
    Criminal Procedure, or is collateral to the preparation of an application
    under Article 11.073, Code of Criminal Procedure, if the case was referred
    in writing to the office by the Texas Forensic Science Commission under
    Section 4(h), Article 38.01, Code of Criminal Procedure.(c)
    Notwithstanding Article 26.04(p), Code of Criminal Procedure, the office
    may independently investigate the financial condition of any person the
    office is appointed to represent.  The office shall report the results of
    the investigation to the appointing judge.  The judge may hold a hearing
    to determine if the person is indigent and entitled to representation
    under this section.(d)  The office may consult with law school clinics
    with applicable knowledge and experience and with other experts as
    necessary to investigate the facts of a particular case.  Added by Acts
    2009, 81st Leg., R.S., Ch. 781 (S.B. 1091), Sec. 1, eff. September 1,
    2009.Amended by: Acts 2015, 84th Leg., R.S., Ch. 1215 (S.B. 1743), Sec.
    20, eff. September 1, 2015.

    Sec. 78.055.  COMPENSATION OF OTHER APPOINTED ATTORNEYS.  If it is
    necessary that an attorney other than an attorney employed by the office
    be appointed, that attorney shall be compensated as provided by Articles
    11.071 and 26.05, Code of Criminal Procedure.  Added by Acts 2009, 81st
    Leg., R.S., Ch. 781 (S.B. 1091), Sec. 1, eff. September 1, 2009.

    Sec. 78.056.  APPOINTMENT LIST.  (a)  The presiding judges of the
    administrative judicial regions shall maintain a statewide list of
    competent counsel available for appointment under Section 2(f), Article
    11.071, Code of Criminal Procedure, if the office does not accept or is
    prohibited from accepting an appointment under Section 78.054.  Each
    attorney on the list:(1)  must exhibit proficiency and commitment to
    providing quality representation to defendants in death penalty cases;
    and(2)  may not have been found by a state or federal court to have
    rendered ineffective assistance of counsel during the trial or appeal of a
    death penalty case.(b)  The Office of Court Administration of the Texas
    Judicial System and the Texas Indigent Defense Commission shall provide
    administrative support necessary under this section.  Added by Acts 2009,
    81st Leg., R.S., Ch. 781 (S.B. 1091), Sec. 1, eff. September 1,
    2009.Amended by: Acts 2011, 82nd Leg., R.S., Ch. 984 (H.B. 1754), Sec. 4,
    eff. September 1, 2011.


                    
