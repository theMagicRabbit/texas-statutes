        
                        
    SPECIAL DISTRICT LOCAL LAWS CODE
    TITLE 6. WATER AND WASTEWATER
    SUBTITLE C. SPECIAL UTILITY DISTRICTS
    CHAPTER 7214.  ROCKETT SPECIAL UTILITY DISTRICT
    SUBCHAPTER A.  GENERAL PROVISIONS
    Sec. 7214.001.  DEFINITION.  In this chapter, "district" means the Rockett Special Utility District.
    Added by Acts 2013, 83rd Leg., R.S., Ch. 386 (H.B. 436), Sec. 1, eff. September 1, 2013.

    SUBCHAPTER B.  BONDS
    Sec. 7214.051.  AUTHORITY TO ISSUE BONDS.  (a)  The district has the rights, powers, duties, and obligations of an issuer under Chapter 1371, Government Code.(b)  Sections 49.181 and 49.182, Water Code, do not apply to the district.
    Added by Acts 2013, 83rd Leg., R.S., Ch. 386 (H.B. 436), Sec. 1, eff. September 1, 2013.


                    
