     
                        
    ALCOHOLIC BEVERAGE CODE
    TITLE 3. LICENSES AND PERMITS
    SUBTITLE A. PERMITS
    This Chapter 57, consisting of Secs. 57.001 to 57.005, was added by Acts 2019, 86th Leg., R.S., Ch. 1161 (H.B. 3222), Sec. 2(b).
    See also another Chapter 57, consisting of Secs. 57.01 to 57.09, as added by Acts 2019, 86th Leg., R.S., Ch. 441 (S.B. 1450), Sec. 3.
    CHAPTER 57.  NONRESIDENT BREWER'S AGENT

    Text of section effective on September 01, 2021
    Sec. 57.001.  AUTHORIZED ACTIVITIES.  A nonresident brewer's agent may:(1)  represent one or more nonresident brewers; and(2)  on behalf of a nonresident brewer whom the agent represents:(A)  perform any activity the nonresident brewer whom the agent represents could perform in this state; and(B)  apply for a permit, license, or other authorization required by the commission.
    Added by Acts 2019, 86th Leg., R.S., Ch. 1161 (H.B. 3222), Sec. 2(b), eff. September 1, 2021.


    Text of section effective on September 01, 2021
    Sec. 57.002.  RESTRICTION AS TO REPRESENTATION.  (a)  A nonresident brewer's agent may not represent a nonresident brewer unless the agent is the primary American source of supply for a product produced by the nonresident brewer.(b)  In this section, "primary American source of supply" means the nonresident brewer or the exclusive agent of the nonresident brewer.  To be the "primary American source of supply" the nonresident brewer's agent must be the first source, that is, the brewer or the source closest to the brewer, in the channel of commerce from whom the product can be secured by Texas distributors.
    Added by Acts 2019, 86th Leg., R.S., Ch. 1161 (H.B. 3222), Sec. 2(b), eff. September 1, 2021.


    Text of section effective on September 01, 2021
    Sec. 57.003.  AUTHORIZATION BY NONRESIDENT BREWER REQUIRED.  A nonresident brewer's agent must be authorized to act as the agent of a nonresident brewer the person proposes to represent.
    Added by Acts 2019, 86th Leg., R.S., Ch. 1161 (H.B. 3222), Sec. 2(b), eff. September 1, 2021.


    Text of section effective on September 01, 2021
    Sec. 57.004.  TERRITORIAL AGREEMENT NOT AFFECTED.  Nothing in this chapter affects a territorial agreement entered into under Subchapter C, Chapter 102.
    Added by Acts 2019, 86th Leg., R.S., Ch. 1161 (H.B. 3222), Sec. 2(b), eff. September 1, 2021.


    Text of section effective on September 01, 2021
    Sec. 57.005.  RESPONSIBILITY FOR AGENT'S ACTIONS.  A nonresident brewer is responsible for any action taken by a nonresident brewer's agent in the course of the agent's representation of the nonresident brewer under this chapter to the same extent and in the same manner as if the action had been taken by the nonresident brewer.
    Added by Acts 2019, 86th Leg., R.S., Ch. 1161 (H.B. 3222), Sec. 2(b), eff. September 1, 2021.


                    
