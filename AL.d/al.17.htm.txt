     
                        
    ALCOHOLIC BEVERAGE CODE
    TITLE 3. LICENSES AND PERMITS
    SUBTITLE A. PERMITS
    CHAPTER 17.  WINERY FESTIVAL PERMIT

    Text of section effective until September 01, 2021
    Sec. 17.01.  AUTHORIZED ACTIVITIES.  (a)  The holder of a winery festival permit may sell wine at a civic or wine festival, farmers' market, celebration, or similar event.(b)  The holder of a winery festival permit may not offer wine for sale under this chapter  on more than four consecutive days at the same location.
    Added by Acts 2009, 81st Leg., R.S., Ch. 1366 (S.B. 711), Sec. 1, eff. September 1, 2009.Amended by: Acts 2011, 82nd Leg., R.S., Ch. 893 (S.B. 438), Sec. 1, eff. June 17, 2011.


    Text of section effective until September 01, 2021
    Sec. 17.02.  QUALIFICATION FOR PERMIT.  A winery festival permit may be issued only to the holder of a winery permit.
    Added by Acts 2009, 81st Leg., R.S., Ch. 1366 (S.B. 711), Sec. 1, eff. September 1, 2009.


    Text of section effective until September 01, 2021
    Sec. 17.03.  NOTICE OF SALES; PROCEDURES.  (a)  Before the holder of a winery festival permit offers wine for sale under this chapter, the permit holder must, in accordance with any rules adopted or procedures established by the commission, notify the commission of the date on which and location where the permit holder will offer wine for sale under this chapter.
    Added by Acts 2009, 81st Leg., R.S., Ch. 1366 (S.B. 711), Sec. 1, eff. September 1, 2009.


    Text of section effective until September 01, 2021
    Sec. 17.04.  PERMIT FEE.  The fee for a winery festival permit is $50.
    Added by Acts 2009, 81st Leg., R.S., Ch. 1366 (S.B. 711), Sec. 1, eff. September 1, 2009.


    Text of section effective until September 01, 2021
    Sec. 17.05.  APPLICABILITY OF OTHER LAW.  (a)  The provisions of this code applicable to the sale of wine on the permitted premises of the holder of a winery permit apply to the sale of wine under this chapter.(b)  The winery permit of the holder of a winery festival permit may be canceled or suspended for a violation occurring in connection with activities conducted under this chapter.
    Added by Acts 2009, 81st Leg., R.S., Ch. 1366 (S.B. 711), Sec. 1, eff. September 1, 2009.


                    
