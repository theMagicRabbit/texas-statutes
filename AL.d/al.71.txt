     
                        
    ALCOHOLIC BEVERAGE CODE
    TITLE 3. LICENSES AND PERMITS
    SUBTITLE B. LICENSES
    CHAPTER 71. RETAIL DEALER'S OFF-PREMISE LICENSE

    Text of section effective until September 01, 2021
    Sec. 71.01.  AUTHORIZED ACTIVITIES.  The holder of a retail dealer's off-premise license may sell beer in lawful containers to consumers, but not for resale and not to be opened or consumed on or near the premises where sold.
    Acts 1977, 65th Leg., p. 485, ch. 194, Sec. 1, eff. Sept. 1, 1977.Amended by: Acts 2019, 86th Leg., R.S., Ch. 1359 (H.B. 1545), Sec. 248, eff. September 1, 2021.


    Text of section effective on September 01, 2021
    Sec. 71.01.  AUTHORIZED ACTIVITIES.  The holder of a retail dealer's off-premise license may sell malt beverages in lawful containers to consumers, but not for resale and not to be opened or consumed on or near the premises where sold.
    Acts 1977, 65th Leg., p. 485, ch. 194, Sec. 1, eff. Sept. 1, 1977.Amended by: Acts 2019, 86th Leg., R.S., Ch. 1359 (H.B. 1545), Sec. 248, eff. September 1, 2021.


    Text of section effective until September 01, 2021
    Sec. 71.02.  FEE.  The annual state fee for a retail dealer's off-premise license is $60.
    Acts 1977, 65th Leg., p. 485, ch. 194, Sec. 1, eff. Sept. 1, 1977.  Amended by Acts 1983, 68th Leg., p. 1351, ch. 278, Sec. 48, eff. Sept. 1, 1983.Amended by: Acts 2019, 86th Leg., R.S., Ch. 1359 (H.B. 1545), Sec. 409(b)(29), eff. September 1, 2021.


    Text of section effective until September 01, 2021
    Sec. 71.03.  AUTHORITY OF LICENSEE HOLDING PACKAGE STORE PERMIT OR WINE ONLY PACKAGE STORE PERMIT.  (a)  The holder of a retail dealer's off-premise license who also holds a package store permit may sell beer directly to consumers by the container, but not for resale and not to be opened or consumed on or near the premises where sold.(b)  The holder of a retail dealer's off-premise license who also holds a wine only package store permit may sell beer to consumers by the container, but not for resale and not to be opened or consumed on or near the premises where sold.(c)  The sale of beer by a holder of a retail dealer's off-premise license who also holds a package store permit is subject to the same restrictions and penalties governing the sale of liquor by package stores with regard to:(1)  the hours of sale and delivery;(2)  blinds and barriers;(3)  employment of persons under the age of 18 or sales and deliveries to minors;(4)  sales and deliveries on Sunday;  and(5)  advertising.(d)  The sale of beer by a holder of a retail dealer's off-premise license who also holds a wine only package store permit is subject to the same restrictions and penalties governing the sale of liquor by package stores with regard to:(1)  blinds and barriers;(2)  employment of persons under the age of 18 or sales and deliveries to minors;(3)  delivery to the licensee or permittee on Sunday;  and(4)  advertising.
    Acts 1977, 65th Leg., p. 485, ch. 194, Sec. 1, eff. Sept. 1, 1977.  Amended by Acts 1979, 66th Leg., p. 54, ch. 33, Sec. 6, eff. Jan. 1, 1980;  Acts 1981, 67th Leg., p. 257, ch. 107, Sec. 7, eff. Sept. 1, 1981;  Acts 1987, 70th Leg., ch. 303, Sec. 2, eff. June 11, 1987.Amended by: Acts 2019, 86th Leg., R.S., Ch. 1359 (H.B. 1545), Sec. 410(a)(16), eff. September 1, 2021.

    Sec. 71.04.  POSSESSION OF CERTAIN BEVERAGES PROHIBITED.  No retail dealer's off-premise licensee, nor his officer, may possess liquor containing alcohol in excess of 14 percent by volume on the licensed premises.
    Acts 1977, 65th Leg., p. 486, ch. 194, Sec. 1, eff. Sept. 1, 1977.

    Sec. 71.05.  ACQUISITION OF BEVERAGES FOR RESALE FROM OTHER LICENSEES PROHIBITED.  No holder of a retail dealer's off-premise license may borrow or acquire from, exchange with, or loan to any other holder of a retail dealer's off-premise license or holder of a retail dealer's on-premise license any alcoholic beverage for the purpose of resale.
    Acts 1977, 65th Leg., p. 486, ch. 194, Sec. 1, eff. Sept. 1, 1977.


    Text of section effective until September 01, 2021
    Sec. 71.06.  STORING OR POSSESSING BEER OFF PREMISES PROHIBITED.  No holder of a retail dealer's off-premise license may own, possess, or store beer for the purpose of resale except on the licensed premises.
    Acts 1977, 65th Leg., p. 486, ch. 194, Sec. 1, eff. Sept. 1, 1977.Amended by: Acts 2019, 86th Leg., R.S., Ch. 1359 (H.B. 1545), Sec. 249, eff. September 1, 2021.


    Text of section effective on September 01, 2021
    Sec. 71.06.  STORING OR POSSESSING MALT BEVERAGES OFF PREMISES PROHIBITED.  A holder of a retail dealer's off-premise license may not own, possess, or store malt beverages for the purpose of resale except on the licensed premises.
    Acts 1977, 65th Leg., p. 486, ch. 194, Sec. 1, eff. Sept. 1, 1977.Amended by: Acts 2019, 86th Leg., R.S., Ch. 1359 (H.B. 1545), Sec. 249, eff. September 1, 2021.


    Text of section effective until September 01, 2021
    Sec. 71.07.  EXCHANGE OR TRANSPORTATION OF BEER BETWEEN LICENSED PREMISES UNDER SAME OWNERSHIP.  Section 69.11 of this code relates to the exchange or transportation of beer between licensed premises by retail dealers.
    Acts 1977, 65th Leg., p. 486, ch. 194, Sec. 1, eff. Sept. 1, 1977.Amended by: Acts 2019, 86th Leg., R.S., Ch. 1359 (H.B. 1545), Sec. 250, eff. September 1, 2021.


    Text of section effective on September 01, 2021
    Sec. 71.07.  EXCHANGE OR TRANSPORTATION OF MALT BEVERAGES BETWEEN LICENSED PREMISES UNDER SAME OWNERSHIP.  Section 69.11 relates to the exchange or transportation of malt beverages between licensed premises by retail dealers.
    Acts 1977, 65th Leg., p. 486, ch. 194, Sec. 1, eff. Sept. 1, 1977.Amended by: Acts 2019, 86th Leg., R.S., Ch. 1359 (H.B. 1545), Sec. 250, eff. September 1, 2021.

    Sec. 71.08.  MITIGATING CIRCUMSTANCES:  RETAIL DEALER'S OFF-PREMISE LICENSE.  Section 11.64 of this code relates to mitigating circumstances with respect to cancellation or suspension of a retail dealer's off-premise license.
    Acts 1977, 65th Leg., p. 486, ch. 194, Sec. 1, eff. Sept. 1, 1977.


    Text of section effective until September 01, 2021
    Sec. 71.09.  BREACH OF PEACE:  RETAIL ESTABLISHMENT.  The application of sanctions for the occurrence of a breach of the peace at a retail beer establishment is covered by Section 69.13 of this code.
    Acts 1977, 65th Leg., p. 487, ch. 194, Sec. 1, eff. Sept. 1, 1977.Amended by: Acts 2019, 86th Leg., R.S., Ch. 1359 (H.B. 1545), Sec. 251, eff. September 1, 2021.


    Text of section effective on September 01, 2021
    Sec. 71.09.  BREACH OF PEACE: RETAIL ESTABLISHMENT.  The application of sanctions for the occurrence of a breach of the peace at a retail malt beverage establishment is covered by Section 69.13.
    Acts 1977, 65th Leg., p. 487, ch. 194, Sec. 1, eff. Sept. 1, 1977.Amended by: Acts 2019, 86th Leg., R.S., Ch. 1359 (H.B. 1545), Sec. 251, eff. September 1, 2021.

    Sec. 71.10.  WARNING SIGN REQUIRED.
    Text of subsection effective until September 01, 2021
      (a)  Each holder of a retail dealer's off-premise license shall display in a prominent place on his premises a sign stating in letters at least two inches high:  IT IS A CRIME (MISDEMEANOR) TO CONSUME LIQUOR OR BEER ON THESE PREMISES.
    Text of subsection effective on September 01, 2021
    (a)  Each holder of a retail dealer's off-premise license shall display in a prominent place on the licensee's premises a sign stating in letters at least two inches high: IT IS A CRIME (MISDEMEANOR) TO CONSUME LIQUOR OR MALT BEVERAGES ON THESE PREMISES.(b)  A licensee who fails to comply with this section commits a misdemeanor punishable by a fine of not more than $25.
    Added by Acts 1983, 68th Leg., p. 2212, ch. 414, Sec. 4, eff. Sept. 1, 1983.Amended by: Acts 2019, 86th Leg., R.S., Ch. 1359 (H.B. 1545), Sec. 252, eff. September 1, 2021.


    Text of section effective until September 01, 2021
    Sec. 71.11.  BEER SAMPLING.  (a)  The holder of a retail dealer's off-premise license may conduct free product samplings of beer on the license holder's premises during regular business hours as provided by this section.(b)  An agent or employee of the holder of a retail dealer's off-premise license may open, touch, or pour beer, make a presentation, or answer questions at a sampling event.(c)  For the purposes of this code and any other law or ordinance:(1)  a retail dealer's off-premise license does not authorize the sale of alcoholic beverages for on-premise consumption; and(2)  none of the license holder's income may be considered to be income from the sale of alcoholic beverages for on-premise consumption.(d)  Any beer used in a sampling event under this section must be purchased from or provided by the retailer on whose premises the sampling event is held.
    Added by Acts 2007, 80th Leg., R.S., Ch. 1073 (H.B. 2723), Sec. 9, eff. September 1, 2007.Amended by: Acts 2019, 86th Leg., R.S., Ch. 1359 (H.B. 1545), Sec. 253, eff. September 1, 2021.


    Text of section effective on September 01, 2021
    Sec. 71.11.  MALT BEVERAGE SAMPLING.  (a)  The holder of a retail dealer's off-premise license may conduct free product samplings of malt beverages on the license holder's premises during regular business hours as provided by this section.(b)  An agent or employee of the holder of a retail dealer's off-premise license may open, touch, or pour malt beverages, make a presentation, or answer questions at a sampling event.(c)  For the purposes of this code and any other law or ordinance:(1)  a retail dealer's off-premise license does not authorize the sale of alcoholic beverages for on-premise consumption; and(2)  none of the license holder's income may be considered to be income from the sale of alcoholic beverages for on-premise consumption.(d)  Any malt beverages used in a sampling event under this section must be purchased from or provided by the retailer on whose premises the sampling event is held.
    Added by Acts 2007, 80th Leg., R.S., Ch. 1073 (H.B. 2723), Sec. 9, eff. September 1, 2007.Amended by: Acts 2019, 86th Leg., R.S., Ch. 1359 (H.B. 1545), Sec. 253, eff. September 1, 2021.


                    
