      
                        
    ALCOHOLIC BEVERAGE CODE
    TITLE 3. LICENSES AND PERMITS
    SUBTITLE B. LICENSES
    CHAPTER 69. RETAIL DEALER'S ON-PREMISE LICENSE

    Text of section effective until September 01, 2021
    Sec. 69.01.  AUTHORIZED ACTIVITIES.  The holder of a retail dealer's on-premise license may sell beer in or from any lawful container to the ultimate consumer for consumption on or off the premises where sold.  The licensee may not sell beer for resale.
    Acts 1977, 65th Leg., p. 482, ch. 194, Sec. 1, eff. Sept. 1, 1977.Amended by: Acts 2019, 86th Leg., R.S., Ch. 1359 (H.B. 1545), Sec. 240, eff. September 1, 2021.


    Text of section effective on September 01, 2021
    Sec. 69.01.  AUTHORIZED ACTIVITIES.  The holder of a retail dealer's on-premise license may sell malt beverages in or from any lawful container to the ultimate consumer for consumption on or off the premises where sold.  The licensee may not sell malt beverages for resale.
    Acts 1977, 65th Leg., p. 482, ch. 194, Sec. 1, eff. Sept. 1, 1977.Amended by: Acts 2019, 86th Leg., R.S., Ch. 1359 (H.B. 1545), Sec. 240, eff. September 1, 2021.


    Text of section effective until September 01, 2021
    Sec. 69.02.  FEE.  (a)  Except as provided in Subsection (b) and Section 69.03, the annual state fee for a retail dealer's on-premise license is $150.(b)  The annual state fee for a retail dealer's on-premise license in connection with an establishment located in a county with a population of 1.4 million or more is $750.  The original application fee for a retail dealer's on-premise license in connection with an establishment located in a county with a population of 1.4 million or more is $1,000.
    Acts 1977, 65th Leg., p. 482, ch. 194, Sec. 1, eff. Sept. 1, 1977.  Amended by Acts 1983, 68th Leg., p. 1351, ch. 278, Sec. 45, eff. Sept. 1, 1983.Amended by: Acts 2005, 79th Leg., Ch. 452 (S.B. 1850), Sec. 8, eff. September 1, 2005.Acts 2019, 86th Leg., R.S., Ch. 1359 (H.B. 1545), Sec. 409(b)(27), eff. September 1, 2021.


    Text of section effective until September 01, 2021
    Sec. 69.03.  ISSUANCE OF LICENSE FOR RAILWAY CARS.  A retail dealer's on-premise license may be issued for a railway dining, buffet, or club car.  Application for a license of this type shall be made directly to the commission, and the annual state fee is $30 for each car.
    Acts 1977, 65th Leg., p. 482, ch. 194, Sec. 1, eff. Sept. 1, 1977.  Amended by Acts 1983, 68th Leg., p. 1351, ch. 278, Sec. 46, eff. Sept. 1, 1983.Amended by: Acts 2019, 86th Leg., R.S., Ch. 1359 (H.B. 1545), Sec. 409(b)(28), eff. September 1, 2021.


    Text of section effective until September 01, 2021
    Sec. 69.04.  HOTELS NOT DISQUALIFIED.  The fact that a hotel holds a permit to sell distilled spirits in unbroken packages does not disqualify the hotel from also obtaining a license to sell beer for on-premises consumption.
    Acts 1977, 65th Leg., p. 482, ch. 194, Sec. 1, eff. Sept. 1, 1977.Amended by: Acts 2019, 86th Leg., R.S., Ch. 1359 (H.B. 1545), Sec. 241, eff. September 1, 2021.


    Text of section effective on September 01, 2021
    Sec. 69.04.  HOTELS NOT DISQUALIFIED.  The fact that a hotel holds a permit to sell distilled spirits in unbroken packages does not disqualify the hotel from also obtaining a license to sell malt beverages for on-premises consumption.
    Acts 1977, 65th Leg., p. 482, ch. 194, Sec. 1, eff. Sept. 1, 1977.Amended by: Acts 2019, 86th Leg., R.S., Ch. 1359 (H.B. 1545), Sec. 241, eff. September 1, 2021.

    Sec. 69.06.  DENIAL OF ORIGINAL APPLICATION.  (a)  The commission shall deny an original application for a retail dealer's on-premise license if the commission finds that the applicant or the applicant's spouse, during the five years immediately preceding the application, was finally convicted of a felony or one of the following offenses:(1)  prostitution;(2)  a vagrancy offense involving moral turpitude;(3)  bookmaking;(4)  gambling or gaming;(5)  an offense involving controlled substances as defined in the Texas Controlled Substances Act, including an offense involving a synthetic cannabinoid, or an offense involving other dangerous drugs;(6)  a violation of this code resulting in the cancellation of a license or permit, or a fine of not less than $500;(7)  more than three violations of this code relating to minors;(8)  bootlegging; or(9)  an offense involving firearms or a deadly weapon.(b)  The commission shall also deny an original application for a license if the commission finds that five years has not elapsed since the termination of a sentence, parole, or probation served by the applicant or the applicant's spouse because of a felony conviction or conviction of any of the offenses described in Subsection (a).(c)  The commission shall deny an application for a renewal of a retail dealer's on-premise license if it finds:(1)  that the applicant or the applicant's spouse has been finally convicted of a felony or one of the offenses listed in Subsection (a) at any time during the five years immediately preceding the filing of the application for renewal; or(2)  that five years has not elapsed since the termination of a sentence, parole, or probation served by the applicant or the applicant's spouse because of a felony prosecution or prosecution for any of the offenses described in Subsection (a).(d)  In this section the word "applicant" includes the individual natural person holding or applying for the license or, if the holder or applicant is not an individual natural person, the individual partner, officer, trustee, or receiver who is primarily responsible for the management of the premises.(e)  In this section, "synthetic cannabinoid" means a substance included in Penalty Group 2-A under Section 481.1031, Health and Safety Code.
    Acts 1977, 65th Leg., p. 483, ch. 194, Sec. 1, eff. Sept. 1, 1977.  Amended by Acts 2003, 78th Leg., ch. 625, Sec. 5, eff. Sept. 1, 2003.Amended by: Acts 2017, 85th Leg., R.S., Ch. 539 (S.B. 341), Sec. 1, eff. September 1, 2017.Acts 2019, 86th Leg., R.S., Ch. 1359 (H.B. 1545), Sec. 242, eff. December 31, 2020.

    Sec. 69.09.  ACQUISITION OF BEVERAGES FOR RESALE FROM OTHER LICENSEES PROHIBITED.  No holder of a retail dealer's on-premise license may borrow or acquire from, exchange with, or loan to any other holder of a retail dealer's on-premise license or holder of a retail dealer's off-premise license any alcoholic beverage for the purpose of resale.
    Acts 1977, 65th Leg., p. 484, ch. 194, Sec. 1, eff. Sept. 1, 1977.


    Text of section effective until September 01, 2021
    Sec. 69.10.  STORING OR POSSESSING BEER OFF PREMISES PROHIBITED.  No holder of a retail dealer's on-premise license may own, possess, or store beer for the purpose of resale except on the licensed premises.
    Acts 1977, 65th Leg., p. 484, ch. 194, Sec. 1, eff. Sept. 1, 1977.Amended by: Acts 2019, 86th Leg., R.S., Ch. 1359 (H.B. 1545), Sec. 243, eff. September 1, 2021.


    Text of section effective on September 01, 2021
    Sec. 69.10.  STORING OR POSSESSING MALT BEVERAGES OFF PREMISES PROHIBITED.  No holder of a retail dealer's on-premise license may own, possess, or store malt beverages for the purpose of resale except on the licensed premises.
    Acts 1977, 65th Leg., p. 484, ch. 194, Sec. 1, eff. Sept. 1, 1977.Amended by: Acts 2019, 86th Leg., R.S., Ch. 1359 (H.B. 1545), Sec. 243, eff. September 1, 2021.


    Text of section effective until September 01, 2021
    Sec. 69.11.  EXCHANGE OR TRANSPORTATION OF BEER BETWEEN LICENSED PREMISES UNDER SAME OWNERSHIP.  The owner of two or more licensed retail premises may not exchange or transport beer between them unless all of the conditions set out in Section 24.04 of this code are met, except that beer may be transferred between two licensed retail premises that are both covered by package store permits as provided in Section 22.08 of this code.
    Acts 1977, 65th Leg., p. 484, ch. 194, Sec. 1, eff. Sept. 1, 1977.Amended by: Acts 2019, 86th Leg., R.S., Ch. 1359 (H.B. 1545), Sec. 244, eff. September 1, 2021.


    Text of section effective on September 01, 2021
    Sec. 69.11.  EXCHANGE OR TRANSPORTATION OF MALT BEVERAGES BETWEEN LICENSED PREMISES UNDER SAME OWNERSHIP.  The owner of two or more licensed retail premises may not exchange or transport malt beverages between them unless all of the conditions set out in Section 24.04 are met, except that malt beverages may be transferred between two licensed retail premises that are both covered by package store permits as provided in Section 22.08.
    Acts 1977, 65th Leg., p. 484, ch. 194, Sec. 1, eff. Sept. 1, 1977.Amended by: Acts 2019, 86th Leg., R.S., Ch. 1359 (H.B. 1545), Sec. 244, eff. September 1, 2021.

    Sec. 69.12.  POSSESSION OF CERTAIN BEVERAGES PROHIBITED.  No retail dealer's on-premise licensee, nor the licensee's officer, agent, servant, or employee, may possess on the licensed premises an alcoholic beverage which is not authorized to be sold on the premises.
    Acts 1977, 65th Leg., p. 484, ch. 194, Sec. 1, eff. Sept. 1, 1977.


    Text of section effective until September 01, 2021
    Sec. 69.13.  BREACH OF PEACE:  RETAIL ESTABLISHMENT.  The commission or administrator may suspend or cancel the license of a retail beer dealer after giving the licensee notice and the opportunity to show compliance with all requirements of law for retention of the license if it finds that a breach of the peace has occurred on the licensed premises or on premises under the licensee's control and that the breach of the peace was not beyond the control of the licensee and resulted from his improper supervision of persons permitted to be on the licensed premises or on premises under his control.
    Acts 1977, 65th Leg., p. 484, ch. 194, Sec. 1, eff. Sept. 1, 1977.Amended by: Acts 2019, 86th Leg., R.S., Ch. 1359 (H.B. 1545), Sec. 245, eff. September 1, 2021.


    Text of section effective on September 01, 2021
    Sec. 69.13.  BREACH OF PEACE: RETAIL ESTABLISHMENT.  The commission or administrator may suspend or cancel the license of a retail malt beverage dealer after giving the licensee notice and the opportunity to show compliance with all requirements of law for retention of the license if it finds that a breach of the peace has occurred on the licensed premises or on premises under the licensee's control and that the breach of the peace was not beyond the control of the licensee and resulted from the licensee's improper supervision of persons permitted to be on the licensed premises or on premises under the licensee's control.
    Acts 1977, 65th Leg., p. 484, ch. 194, Sec. 1, eff. Sept. 1, 1977.Amended by: Acts 2019, 86th Leg., R.S., Ch. 1359 (H.B. 1545), Sec. 245, eff. September 1, 2021.

    Sec. 69.14.  SEATING AREA REQUIRED.  A retail dealer's on-premise licensee must have an area designated on the premises for the permittee's customers to sit if they wish to consume beverages sold by the licensee on the premises.
    Added by Acts 1983, 68th Leg., p. 2211, ch. 414, Sec. 2, eff. Sept. 1, 1983.

    Sec. 69.15.  RESTRICTIONS ON LOCATION IN CERTAIN MUNICIPALITIES.  (a)  Section 11.52 of this code applies to the issuance of a retail dealer's on-premise license as if the license were a permit to which this section applies.(b)  Section 61.31(b) of this code does not apply to an application for a retail dealer's on-premise license.
    Added by Acts 1993, 73rd Leg., ch. 934, Sec. 56, eff. Sept. 1, 1993.

    Sec. 69.16.  FOOD AND BEVERAGE CERTIFICATE.  (a)  In this section, "location" means the designated physical address of the retail dealer's on-premise license and includes all areas at the address where the license holder may sell or deliver alcoholic beverages for immediate consumption regardless of whether some of those areas are occupied by other businesses.(a-1)  A holder of a retail dealer's on-premise license may be issued a food and beverage certificate by the commission if the commission finds that the receipts from the sale of alcoholic beverages by the license holder at the location are 60 percent or less of the total receipts from the location.(b)  A food and beverage certificate may not be issued unless the location has permanent food service facilities for the preparation and service of multiple entrees for consumption at the location.(b-1)  The commission shall adopt rules requiring the holder of a food and beverage certificate to assure that permanent food service facilities for the preparation and service of multiple entrees for consumption at the location are available at the location.  The commission may exempt licensees who are concessionaires in public entertainment venues such as sports stadiums and convention centers from Subsections (a-1) and (b).(c)  The fee for a food and beverage certificate shall be set at a level sufficient to recover the cost of issuing the certificate and administering this section.(d)  A certificate issued under this section expires on the expiration of the primary retail dealer's on-premise license.  A certificate may be canceled at any time, and the renewal of a certificate may be denied, if the commission finds that the holder of the certificate is in violation of Subsection (a-1) or (b) or a rule adopted under Subsection (b-1).  On finding that the licensee knowingly operated under a food and beverage certificate while not complying with this section or a rule adopted under Subsection (b-1), the commission may cancel or deny the renewal of the licensee's retail dealer's on-premise license.  The  holder of a retail dealer's on-premise license whose certificate has been canceled or who is denied renewal of a certificate under this subsection may not apply for a new certificate until the day after the first anniversary of the date the certificate was canceled or the renewal of the certificate was denied.(e)  Section 61.13 does not apply to the holder of a food and beverage certificate.
    Added by Acts 1995, 74th Leg., ch. 1060, Sec. 8, eff. Aug. 28, 1995.  Amended by Acts 2001, 77th Leg., ch. 853, Sec. 4, eff. Sept. 1, 2001;  Acts 2001, 77th Leg., ch. 1045, Sec. 4, eff. Sept. 1, 2001.Amended by: Acts 2007, 80th Leg., R.S., Ch. 1384 (S.B. 1426), Sec. 2, eff. September 1, 2007.Acts 2017, 85th Leg., R.S., Ch. 466 (H.B. 2101), Sec. 4, eff. September 1, 2017.

    Sec. 69.17.  ISSUANCE OF LICENSE AUTHORIZED FOR CERTAIN AREAS.
    Text of subsection effective until September 01, 2021
      (a) Notwithstanding any other provision of this code, a license under this chapter may be issued for a premises in an area in which the voters have approved the following alcoholic beverage ballot issues in a local option election:(1)  "The legal sale of beer and wine for off-premise consumption only."; and(2)  either:(A)  "The legal sale of mixed beverages."; or(B)  "The legal sale of mixed beverages in restaurants by food and beverage certificate holders only."
    Text of subsection effective on September 01, 2021
    (a)  Notwithstanding any other provision of this code, a license under this chapter may be issued for a premises in an area in which the voters have approved the following alcoholic beverage ballot issues in a local option election:(1)  "The legal sale of malt beverages and wine for off-premise consumption only."; and(2)  either:(A)  "The legal sale of mixed beverages."; or(B)  "The legal sale of mixed beverages in restaurants by food and beverage certificate holders only."(b)  A premises that qualifies for a license under this chapter because it is located in an area that approved the ballot issue described by Subsection (a)(2)(B) may be issued a license under this chapter only if the premises is issued a food and beverage certificate.
    Added by Acts 2013, 83rd Leg., R.S., Ch. 1298 (H.B. 2818), Sec. 2, eff. September 1, 2013.Amended by: Acts 2019, 86th Leg., R.S., Ch. 1359 (H.B. 1545), Sec. 246, eff. September 1, 2021.


    Text of section effective on September 01, 2021
    Sec. 69.18.  SALES AT TEMPORARY LOCATION.  (a) The holder of a retail dealer's on-premise license may temporarily sell malt beverages in or from any lawful container to ultimate consumers:(1)  at a picnic, celebration, or similar event; and(2)  in the county where the license is issued.(b)  The holder of a retail dealer's on-premise license may temporarily sell malt beverages under this section for not more than four consecutive days at the same location.(c)  The commission shall adopt rules to implement this section, including rules that:(1)  require the license holder to notify the commission of the dates on which and location where the license holder will temporarily offer malt beverages for sale under this section;(2)  establish a procedure to verify the wet or dry status of the location where the license holder intends to temporarily sell malt beverages under this section;(3)  detail the circumstances when a license holder may temporarily sell malt beverages under this section with just a notification to the commission and the circumstances that require the commission's preapproval before a license holder may temporarily sell malt beverages under this section; and(4)  require the license holder to provide any other information the commission determines necessary.
    Added by Acts 2019, 86th Leg., R.S., Ch. 1359 (H.B. 1545), Sec. 247, eff. September 1, 2021.


                    
