      
                        
    ALCOHOLIC BEVERAGE CODE
    TITLE 3. LICENSES AND PERMITS
    SUBTITLE B. LICENSES
    Text of chapter effective until September 1, 2021
    CHAPTER 62A.  MANUFACTURER'S SELF-DISTRIBUTION LICENSE

    Text of section effective until September 01, 2021
    Sec. 62A.01.  ELIGIBILITY FOR LICENSE.  A manufacturer's self-distribution license may be issued only to the holder of a manufacturer's license under Chapter 62 or the holder of a nonresident manufacturer's license under Chapter 63.
    Added by Acts 2013, 83rd Leg., R.S., Ch. 534 (S.B. 517), Sec. 2, eff. June 14, 2013.Amended by: Acts 2019, 86th Leg., R.S., Ch. 1359 (H.B. 1545), Sec. 222, eff. September 1, 2021.


    Text of section effective until September 01, 2021
    Sec. 62A.02.  AUTHORIZED ACTIVITIES.  (a)  A holder of a manufacturer's self-distribution license whose annual production of beer under the manufacturer's or nonresident manufacturer's license, together with the annual production of ale by the holder of a brewer's or nonresident brewer's permit at all premises owned directly or indirectly by the license holder or an affiliate or subsidiary of the license holder, does not exceed 125,000 barrels may sell beer produced under the manufacturer's or nonresident manufacturer's license to those persons to whom the holder of a general distributor's license may sell beer under Section 64.01(a)(2).(b)  The total combined sales of beer under this section, together with the sales of ale by the holder of a brewer's self-distribution permit under Section 12A.02 at all premises owned directly or indirectly by the license holder or an affiliate or subsidiary of the license holder, may not exceed 40,000 barrels annually.(c)  With regard to a sale under this section, the holder of a manufacturer's self-distribution license has the same authority and is subject to the same requirements that apply to a sale made by the holder of a general distributor's license.(d)  Beer sold under this section may be shipped only from a manufacturing facility in this state.
    Added by Acts 2013, 83rd Leg., R.S., Ch. 534 (S.B. 517), Sec. 2, eff. June 14, 2013.Amended by: Acts 2017, 85th Leg., R.S., Ch. 1129 (H.B. 3287), Sec. 5, eff. June 15, 2017.Acts 2019, 86th Leg., R.S., Ch. 1359 (H.B. 1545), Sec. 223, eff. September 1, 2021.


    Text of section effective until September 01, 2021
    Sec. 62A.03.  FEE.  The annual state fee for a manufacturer's self-distribution license is $250.
    Added by Acts 2013, 83rd Leg., R.S., Ch. 534 (S.B. 517), Sec. 2, eff. June 14, 2013.Amended by: Acts 2019, 86th Leg., R.S., Ch. 1359 (H.B. 1545), Sec. 409(b)(23), eff. September 1, 2021.


    Text of section effective until September 01, 2021
    Sec. 62A.04.  REPORT OF SALES TO RETAILERS.  (a)  Not later than the 15th day of each month, the holder of a manufacturer's self-distribution license shall file a report with the commission that contains information relating to the sales made by the license holder to a retailer during the preceding calendar month.(b)  The commission shall by rule determine the information that is required to be reported under this section and the manner in which the report must be submitted to the commission.  The commission may require the report to contain the same information reported to the comptroller under Section 151.462, Tax Code.
    Added by Acts 2013, 83rd Leg., R.S., Ch. 534 (S.B. 517), Sec. 2, eff. June 14, 2013.Amended by: Acts 2019, 86th Leg., R.S., Ch. 1359 (H.B. 1545), Sec. 224, eff. September 1, 2021.


                    
