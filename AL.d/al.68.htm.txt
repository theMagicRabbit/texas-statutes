        
                        
    ALCOHOLIC BEVERAGE CODE
    TITLE 3. LICENSES AND PERMITS
    SUBTITLE B. LICENSES
    CHAPTER 68. IMPORTER'S CARRIER'S LICENSE

    Text of section effective until September 01, 2021
    Sec. 68.01.  AUTHORIZED ACTIVITIES.  An importer who holds an importer's carrier's license may import beer into this state in vehicles owned or leased in good faith by him.
    Acts 1977, 65th Leg., p. 481, ch. 194, Sec. 1, eff. Sept. 1, 1977.


    Text of section effective until September 01, 2021
    Sec. 68.02.  FEE.  The fee for an importer's carrier's license is $20 per year or fraction of a year.
    Acts 1977, 65th Leg., p. 481, ch. 194, Sec. 1, eff. Sept. 1, 1977.  Amended by Acts 1983, 68th Leg., p. 1350, ch. 278, Sec. 44, eff. Sept. 1, 1983.


    Text of section effective until September 01, 2021
    Sec. 68.03.  ELIGIBILITY FOR LICENSE.  An importer's carrier's license may be issued only to a holder of an importer's license.
    Acts 1977, 65th Leg., p. 481, ch. 194, Sec. 1, eff. Sept. 1, 1977.


    Text of section effective until September 01, 2021
    Sec. 68.04.  APPLICATION FOR LICENSE;  DESCRIPTION OF VEHICLES.  (a)  An application for an importer's carrier's license must contain a description of the vehicles to be used and other information required by the commission.(b)  An importer may not import beer into the state in any vehicle not fully described in his application, except as permitted in Section 67.01 of this code.
    Acts 1977, 65th Leg., p. 481, ch. 194, Sec. 1, eff. Sept. 1, 1977.


    Text of section effective until September 01, 2021
    Sec. 68.05.  EXPIRATION OF LICENSE.  An importer's carrier's license expires at the same time as the holder's primary importer's license.
    Acts 1977, 65th Leg., p. 481, ch. 194, Sec. 1, eff. Sept. 1, 1977.


    Text of section effective until September 01, 2021
    Sec. 68.06.  DESIGNATION OF VEHICLES.  All vehicles used under an importer's carrier's license must have painted or printed on them the designation required by the commission.
    Acts 1977, 65th Leg., p. 482, ch. 194, Sec. 1, eff. Sept. 1, 1977.


                    
