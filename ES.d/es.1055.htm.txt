        
                        
    ESTATES CODE
    TITLE 3. GUARDIANSHIP AND RELATED PROCEDURES
    SUBTITLE C. PROCEDURAL MATTERS
    CHAPTER 1055. TRIAL AND HEARING MATTERS
    SUBCHAPTER A.  STANDING AND PLEADINGS
    Sec. 1055.001.  STANDING TO COMMENCE OR CONTEST PROCEEDING.  (a)  Except as provided by Subsection (b), any person has the right to:(1)  commence a guardianship proceeding, including a proceeding for complete restoration of a ward's capacity or modification of a ward's guardianship; or(2)  appear and contest a guardianship proceeding or the appointment of a particular person as guardian.(b)  A person who has an interest that is adverse to a proposed ward or incapacitated person may not:(1)  file an application to create a guardianship for the proposed ward or incapacitated person;(2)  contest the creation of a guardianship for the proposed ward or incapacitated person;(3)  contest the appointment of a person as a guardian of the proposed ward or incapacitated person; or(4)  contest an application for complete restoration of a ward's capacity or modification of a ward's guardianship.(c)  The court shall determine by motion in limine the standing of a person who has an interest that is adverse to a proposed ward or incapacitated person.
    Added by Acts 2011, 82nd Leg., R.S., Ch. 823 (H.B. 2759), Sec. 1.02, eff. January 1, 2014.

    Sec. 1055.002.  DEFECT IN PLEADING.  A court may not invalidate a pleading in a guardianship proceeding, or an order based on the pleading, on the basis of a defect of form or substance in the pleading unless a timely objection has been made against the defect and the defect has been called to the attention of the court in which the proceeding was or is pending.
    Added by Acts 2011, 82nd Leg., R.S., Ch. 823 (H.B. 2759), Sec. 1.02, eff. January 1, 2014.Amended by: Acts 2013, 83rd Leg., R.S., Ch. 161 (S.B. 1093), Sec. 6.031, eff. January 1, 2014.

    Sec. 1055.003.  INTERVENTION BY INTERESTED PERSON.  (a)  Notwithstanding the Texas Rules of Civil Procedure and except as provided by Subsection (d), an interested person may intervene in a guardianship proceeding only by filing a timely motion to intervene that is served on the parties.(b)  The motion must state the grounds for intervention in the proceeding and be accompanied by a pleading that sets out the purpose for which intervention is sought.(c)  The court has the discretion to grant or deny the motion and, in exercising that discretion, must consider whether:(1)  the intervention will unduly delay or prejudice the adjudication of the original parties' rights; or(2)  the proposed intervenor has such an adverse relationship with the ward or proposed ward that the intervention would unduly prejudice the adjudication of the original parties' rights.(d)  A person who is entitled to receive notice under Section 1051.104 is not required to file a motion under this section to intervene in a guardianship proceeding.
    Added by Acts 2015, 84th Leg., R.S., Ch. 1031 (H.B. 1438), Sec. 7, eff. September 1, 2015.Amended by: Acts 2017, 85th Leg., R.S., Ch. 514 (S.B. 39), Sec. 7, eff. September 1, 2017.

    SUBCHAPTER B.  TRIAL AND HEARING
    Sec. 1055.051.  HEARING BY SUBMISSION.  (a)  A court may consider by submission a motion or application filed under this title unless the proceeding is:(1)  contested; or(2)  an application for the appointment of a guardian.(b)  The party seeking relief under a motion or application being considered by the court on submission has the burden of proof at the hearing.(c)  The court may consider a person's failure to file a response to a motion or application that may be considered on submission as a representation that the person does not oppose the motion or application.(d)  A person's request for oral argument is not a response to a motion or application under this section.(e)  The court, on the court's own motion, may order oral argument on a motion or application that may be considered by submission.
    Added by Acts 2011, 82nd Leg., R.S., Ch. 823 (H.B. 2759), Sec. 1.02, eff. January 1, 2014.

    Sec. 1055.052.  TRIAL BY JURY.  A party in a contested guardianship proceeding is entitled to a jury trial on request.
    Added by Acts 2011, 82nd Leg., R.S., Ch. 823 (H.B. 2759), Sec. 1.02, eff. January 1, 2014.

    Sec. 1055.053.  LOCATION OF HEARING.  (a)  Except as provided by Subsection (b), the judge may hold a hearing on a guardianship proceeding involving an adult ward or adult proposed ward at any suitable location in the county in which the guardianship proceeding is pending.  The hearing should be held in a physical setting that is not likely to have a harmful effect on the ward or proposed ward.(b)  On the request of the adult proposed ward, the adult ward, or the attorney of the proposed ward or ward, the hearing may not be held under the authority of this section at a place other than the courthouse.
    Added by Acts 2013, 83rd Leg., R.S., Ch. 161 (S.B. 1093), Sec. 6.032, eff. January 1, 2014.

    SUBCHAPTER C.  EVIDENCE
    Sec. 1055.101.  APPLICABILITY OF CERTAIN RULES RELATING TO WITNESSES AND EVIDENCE.  The rules relating to witnesses and evidence that apply in the district court apply in a guardianship proceeding to the extent practicable.
    Added by Acts 2011, 82nd Leg., R.S., Ch. 823 (H.B. 2759), Sec. 1.02, eff. January 1, 2014.

    Sec. 1055.102.  USE OF CERTAIN RECORDS AS EVIDENCE.  The following are admissible as evidence in any court of this state:(1)  record books described by Sections 1052.001, 1052.002, and 1052.003 and individual case files described by Section 1052.052, including records maintained in a manner allowed under Section 1052.004; and(2)  certified copies or reproductions of the records.
    Added by Acts 2011, 82nd Leg., R.S., Ch. 823 (H.B. 2759), Sec. 1.02, eff. January 1, 2014.

    SUBCHAPTER D.  MEDIATION
    Sec. 1055.151.  MEDIATION OF CONTESTED GUARDIANSHIP PROCEEDING.  (a)  On the written agreement of the parties or on the court's own motion, the court may refer a contested guardianship proceeding to mediation.(b)  A mediated settlement agreement is binding on the parties if the agreement:(1)  provides, in a prominently displayed statement that is in boldfaced type, in capital letters, or underlined, that the agreement is not subject to revocation by the parties;(2)  is signed by each party to the agreement; and(3)  is signed by the party's attorney, if any, who is present at the time the agreement is signed.(c)  If a mediated settlement agreement meets the requirements of this section, a party is entitled to judgment on the mediated settlement agreement notwithstanding Rule 11, Texas Rules of Civil Procedure, or another rule or law.(d)  Notwithstanding Subsections (b) and (c), a court may decline to enter a judgment on a mediated settlement agreement if the court finds that the agreement is not in the ward's or proposed ward's best interests.
    Added by Acts 2013, 83rd Leg., R.S., Ch. 982 (H.B. 2080), Sec. 7, eff. January 1, 2014.


                    
