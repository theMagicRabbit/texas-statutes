      
                        
    ESTATES CODE
    TITLE 2.  ESTATES OF DECEDENTS; DURABLE POWERS OF ATTORNEY
    SUBTITLE C.  PASSAGE OF TITLE AND DISTRIBUTION OF DECEDENTS' PROPERTY IN GENERAL
    CHAPTER 122.  DISCLAIMERS AND ASSIGNMENTS
    SUBCHAPTER A.  DISCLAIMER OF INTEREST OR POWER
    Sec. 122.001.  DEFINITIONS.  In this subchapter:(1)  "Beneficiary" includes a person who would have been entitled, if the person had not made a disclaimer, to receive property as a result of the death of another person:(A)  by inheritance;(B)  under a will;(C)  by an agreement between spouses for community property with a right of survivorship;(D)  by a joint tenancy with a right of survivorship;(E)  by a survivorship agreement, account, or interest in which the interest of the decedent passes to a surviving beneficiary;(F)  by an insurance, annuity, endowment, employment, deferred compensation, or other contract or arrangement;(G)  under a pension, profit sharing, thrift, stock bonus, life insurance, survivor income, incentive, or other plan or program providing retirement, welfare, or fringe benefits with respect to an employee or a self-employed individual;(H)  by a transfer on death deed; or(I)  by a beneficiary designation as defined by Section 115.001.(2)  "Disclaim" and "disclaimer" have the meanings assigned by Section 240.002, Property Code.
    Added by Acts 2009, 81st Leg., R.S., Ch. 680, Sec. 1, eff. January 1, 2014.Amended by: Acts 2015, 84th Leg., R.S., Ch. 562 (H.B. 2428), Sec. 3, eff. September 1, 2015.Acts 2015, 84th Leg., R.S., Ch. 841 (S.B. 462), Sec. 3, eff. September 1, 2015.Acts 2017, 85th Leg., R.S., Ch. 586 (S.B. 869), Sec. 2, eff. September 1, 2017.

    Sec. 122.002.  DISCLAIMER.   A person who may be entitled to receive property as a beneficiary may disclaim the person's interest in or power over the property in accordance with Chapter 240, Property Code.
    Added by Acts 2009, 81st Leg., R.S., Ch. 680, Sec. 1, eff. January 1, 2014.Amended by: Acts 2015, 84th Leg., R.S., Ch. 562 (H.B. 2428), Sec. 3, eff. September 1, 2015.

    SUBCHAPTER E.  ASSIGNMENT OF INTEREST
    Sec. 122.201.  ASSIGNMENT.  A person who is entitled to receive property or an interest in property from a decedent under a will, by inheritance, or as a beneficiary under a life insurance contract, and does not disclaim the property under Chapter 240, Property Code, may assign the property or interest in property to any person.
    Added by Acts 2009, 81st Leg., R.S., Ch. 680, Sec. 1, eff. January 1, 2014.Amended by: Acts 2015, 84th Leg., R.S., Ch. 562 (H.B. 2428), Sec. 4, eff. September 1, 2015.

    Sec. 122.202.  FILING OF ASSIGNMENT.  An assignment may, at the request of the assignor, be delivered or filed as provided for the delivery or filing of a disclaimer under Subchapter C, Chapter 240, Property Code.
    Added by Acts 2009, 81st Leg., R.S., Ch. 680, Sec. 1, eff. January 1, 2014.Amended by: Acts 2015, 84th Leg., R.S., Ch. 562 (H.B. 2428), Sec. 5, eff. September 1, 2015.

    Sec. 122.204.  FAILURE TO COMPLY.  Failure to comply with Chapter 240, Property Code, does not affect an assignment.
    Added by Acts 2009, 81st Leg., R.S., Ch. 680, Sec. 1, eff. January 1, 2014.Amended by: Acts 2015, 84th Leg., R.S., Ch. 562 (H.B. 2428), Sec. 6, eff. September 1, 2015.

    Sec. 122.205.  GIFT.  An assignment under this subchapter is a gift to the assignee and is not a disclaimer under Chapter 240, Property Code.
    Added by Acts 2009, 81st Leg., R.S., Ch. 680, Sec. 1, eff. January 1, 2014.Amended by: Acts 2015, 84th Leg., R.S., Ch. 562 (H.B. 2428), Sec. 7, eff. September 1, 2015.

    Sec. 122.206.  SPENDTHRIFT PROVISION.  An assignment of property or interest that would defeat a spendthrift provision imposed in a trust may not be made under this subchapter.
    Added by Acts 2009, 81st Leg., R.S., Ch. 680, Sec. 1, eff. January 1, 2014.


                    
