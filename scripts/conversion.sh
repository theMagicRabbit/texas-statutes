#!/bin/sh

for dir in $(ls -ld *.d | awk -F " " '{print $9}');
do
  for file in $(ls -l $dir/*.htm | awk -F " " '{print $9}');
  do
    pandoc -f html -t plain -o $file.txt $file
    rm $file
  done
done
