#!/bin/bash

for file in $1/*.htm.txt; do
  mv -iv "$file" "${file/%.htm.txt/.txt}";
done
