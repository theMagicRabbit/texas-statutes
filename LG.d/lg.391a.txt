       
                        
    LOCAL GOVERNMENT CODE
    TITLE 12. PLANNING AND DEVELOPMENT
    SUBTITLE C. PLANNING AND DEVELOPMENT PROVISIONS APPLYING TO MORE THAN ONE TYPE OF LOCAL GOVERNMENT
    For expiration of this chapter, see Section 391A.006.
    CHAPTER 391A. STORMWATER CONTROL AND RECAPTURE PLANNING AUTHORITIES IN CERTAIN COUNTIES
    Sec. 391A.001.  DEFINITIONS.  In this chapter:(1)  "Affected county" means a county that:(A)  has a population of 800,000 or more; and(B)  receives an average annual rainfall of 15 inches or less based on the most recent 10-year period according to data available from a reliable source, including the United States Department of Agriculture Natural Resources Conservation Service or the PRISM Climate Group, Oregon State University.(2)  "Authority" means a commission established under this chapter.
    Added by Acts 2015, 84th Leg., R.S., Ch. 378 (H.B. 995), Sec. 1, eff. June 10, 2015.

    Sec. 391A.002.  ESTABLISHMENT.  (a) A stormwater control and recapture planning authority is established in each affected county in this state.(b)  An authority is a political subdivision of this state.
    Added by Acts 2015, 84th Leg., R.S., Ch. 378 (H.B. 995), Sec. 1, eff. June 10, 2015.

    Sec. 391A.003.  TERRITORY.  The territory of an authority includes all of the territory in the affected county in which the authority is located except any territory within the boundaries or extraterritorial jurisdiction of that county's largest municipality, provided that the municipality has a plan in place for the control of stormwater on the date the authority is established.
    Added by Acts 2015, 84th Leg., R.S., Ch. 378 (H.B. 995), Sec. 1, eff. June 10, 2015.

    Sec. 391A.004.  BOARD OF DIRECTORS.  (a)  The governing body of an authority is a board of directors composed of:(1)  a representative of the county in which the authority is located and each municipality within the territory of the authority;(2)  a representative of each water utility within the territory of the authority not also described by Subdivision (1);(3)  a representative of each water district within the territory of the authority that has been in operation for at least 15 years;(4)  a member appointed by each member of the state legislature whose legislative district is wholly or partly in the territory of the authority; and(5)  a representative of the Texas Department of Transportation appointed by the Texas Transportation Commission.(b)  A person may not serve as a director if the person holds another public office.
    Added by Acts 2015, 84th Leg., R.S., Ch. 378 (H.B. 995), Sec. 1, eff. June 10, 2015.Amended by: Acts 2017, 85th Leg., R.S., Ch. 792 (H.B. 2725), Sec. 1, eff. June 15, 2017.

    Sec. 391A.005.  POWERS AND DUTIES.  (a)  An authority shall:(1)  coordinate and adopt a long-range master plan to facilitate the development and management of integrated stormwater control and recapture projects and facilities within the authority's territory;(2)  apply for, accept, and receive gifts, grants, loans, and other money available from any source, including the state, the federal government, and an entity represented on the board of directors under Sections 391A.004(1), (2), and (3), to perform its purposes; and (3)  assist an entity represented on the board of directors under Sections 391A.004(1), (2), and (3) in carrying out an objective included in the authority's master plan.(b)  The authority may:(1)  enter into contracts as necessary to carry out the authority's powers and duties; and(2)  employ staff and consult with and retain experts.(c)  The authority may not:(1)  impose a tax or issue bonds; or(2)  regulate the structures or facilities of an electric utility as "electric utility" is defined by Section 31.002, Utilities Code.
    Added by Acts 2015, 84th Leg., R.S., Ch. 378 (H.B. 995), Sec. 1, eff. June 10, 2015.

    Sec. 391A.006.  EXPIRATION OF CHAPTER.  This chapter expires September 1, 2023.
    Added by Acts 2015, 84th Leg., R.S., Ch. 378 (H.B. 995), Sec. 1, eff. June 10, 2015.


                    
