     
                        
    VERNON'S CIVIL STATUTES
    TITLE 71. HEALTH--PUBLIC
    CHAPTER 6-1/2. ABORTION
    Art. 4512.5. DESTROYING UNBORN CHILD.  Whoever shall during parturition of the mother destroy the vitality or life in a child in a state of being born and before actual birth, which child would otherwise have been born alive, shall be confined in the penitentiary for life or for not less than five years. 
                    
