     
                        
    UTILITIES CODE
    TITLE 3. GAS REGULATION
    SUBTITLE A. GAS UTILITY REGULATORY ACT
    CHAPTER 105. JUDICIAL REVIEW;  ENFORCEMENT AND PENALTIES
    SUBCHAPTER A. JUDICIAL REVIEW
    Sec. 105.001.  RIGHT TO JUDICIAL REVIEW.  (a)  Any party to a proceeding before the railroad commission is entitled to judicial review under the substantial evidence rule.(b)  The issue of confiscation is determined by a preponderance of the evidence.
    Acts 1997, 75th Leg., ch. 166, Sec. 1, eff. Sept. 1, 1997.

    Sec. 105.002.  JUDICIAL STAY OR SUSPENSION.  While an appeal of an order, ruling, or decision of a regulatory authority is pending, the district court, court of appeals, or supreme court, as appropriate, may stay or suspend all or part of the operation of the order, ruling, or decision.  In granting or refusing a stay or suspension, the court shall act in accordance with the practice of a court exercising equity jurisdiction.
    Acts 1997, 75th Leg., ch. 166, Sec. 1, eff. Sept. 1, 1997.

    SUBCHAPTER B. ENFORCEMENT AND PENALTIES
    Sec. 105.021.  ACTION TO ENJOIN OR REQUIRE COMPLIANCE.  (a)  The attorney general, on the request of the railroad commission, shall apply in the name of the commission for an order under Subsection (b) if the commission determines that a gas utility or other person is:(1)  engaging in or about to engage in an act that violates this subtitle or an order or rule of the commission entered or adopted under this subtitle;  or(2)  failing to comply with the requirements of this subtitle or a rule or order of the commission.(b)  A court, in an action under this section, may:(1)  prohibit the commencement or continuation of an act that violates this subtitle or an order or rule of the commission entered or adopted under this subtitle;  or(2)  require compliance with a provision of this subtitle or an order or rule of the commission.(c)  The remedy under this section is in addition to any other remedy provided under this subtitle.
    Acts 1997, 75th Leg., ch. 166, Sec. 1, eff. Sept. 1, 1997.

    Sec. 105.022.  CONTEMPT.  The railroad commission may file an action for contempt against a person who:(1)  fails to comply with a lawful order of the commission;(2)  fails to comply with a subpoena or subpoena duces tecum;  or(3)  refuses to testify about a matter on which the person may be lawfully interrogated.
    Acts 1997, 75th Leg., ch. 166, Sec. 1, eff. Sept. 1, 1997.

    Sec. 105.023.  CIVIL PENALTY AGAINST GAS UTILITY OR AFFILIATE.  (a)  A gas utility or affiliate is subject to a civil penalty if the gas utility or affiliate knowingly violates this subtitle, fails to perform a duty imposed on it, or fails, neglects, or refuses to obey an order, rule, direction, or requirement of the railroad commission or a decree or judgment of a court.(b)  A civil penalty under this section shall be in an amount of not less than $1,000 and not more than $5,000 for each violation.(c)  A gas utility or affiliate commits a separate violation each day it continues to violate Subsection (a).(d)  The attorney general shall file in the name of the railroad commission a suit on the attorney general's own initiative or at the request of the commission to recover the civil penalty under this section.
    Acts 1997, 75th Leg., ch. 166, Sec. 1, eff. Sept. 1, 1997.

    Sec. 105.024.  OFFENSE.  (a)  A person commits an offense if the person knowingly violates this subtitle.(b)  An offense under this section is a felony of the third degree.
    Acts 1997, 75th Leg., ch. 166, Sec. 1, eff. Sept. 1, 1997.

    Sec. 105.025.  PLACE FOR SUIT.  A suit for an injunction or a penalty under this subtitle may be brought in:(1)  Travis County;(2)  a county in which the violation is alleged to have occurred;  or(3)  a county in which a defendant resides.
    Acts 1997, 75th Leg., ch. 166, Sec. 1, eff. Sept. 1, 1997.

    Sec. 105.026.  PENALTIES CUMULATIVE.  (a)  A penalty that accrues under this subtitle is cumulative of any other penalty.(b)  A suit for the recovery of a penalty does not bar or affect the recovery of any other penalty or bar a criminal prosecution against any person, including a gas utility or officer, director, agent, or employee of a gas utility.
    Acts 1997, 75th Leg., ch. 166, Sec. 1, eff. Sept. 1, 1997.

    Sec. 105.027.  DISPOSITION OF FINES AND PENALTIES.  A fine or penalty collected under this subtitle, other than a fine or penalty collected in a criminal proceeding, shall be paid to the railroad commission.
    Acts 1997, 75th Leg., ch. 166, Sec. 1, eff. Sept. 1, 1997.

    SUBCHAPTER C. COMPLAINTS
    Sec. 105.051.  COMPLAINT BY AFFECTED PERSON.  An affected person may complain to the regulatory authority in writing setting forth an act or omission by a gas utility in violation or claimed violation of a law that the regulatory authority has jurisdiction to administer or of an order, ordinance, or rule of the regulatory authority.
    Acts 1997, 75th Leg., ch. 166, Sec. 1, eff. Sept. 1, 1997.


                    
