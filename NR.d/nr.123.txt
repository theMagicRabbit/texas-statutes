       
                        
    NATURAL RESOURCES CODE
    TITLE 3. OIL AND GAS
    SUBTITLE D. REGULATION OF SPECIFIC BUSINESSES AND OCCUPATIONS
    CHAPTER 123.  TREATMENT AND RECYCLING FOR BENEFICIAL USE OF DRILL CUTTINGS
    Sec. 123.001.  DEFINITIONS.  In this chapter:(1)  "Commission" means the Railroad Commission of Texas.(2)  "Drill cuttings" means bits of rock or soil cut from a subsurface formation by a drill bit during the process of drilling an oil or gas well and lifted to the surface by means of the circulation of drilling mud.(3)  "Permit holder" means a person who holds a permit from the commission to operate a stationary commercial solid oil and gas waste recycling facility.(4)  "Treatment" means a manufacturing, mechanical, thermal, or chemical process other than sizing, shaping, diluting, or sorting.
    Added by Acts 2015, 84th Leg., R.S., Ch. 351 (H.B. 1331), Sec. 3, eff. September 1, 2015.Amended by: Acts 2017, 85th Leg., R.S., Ch. 184 (S.B. 1541), Sec. 1, eff. May 26, 2017.

    Sec. 123.0015.  BENEFICIAL USE.  (a)  For the purposes of this chapter, a use of drill cuttings is considered to be beneficial if the cuttings are used:(1)  in the construction of oil and gas lease pads or oil and gas lease roads; or(2)  as part of a legitimate commercial product.(b)  The commission by rule shall define "legitimate commercial product" for the purposes of this chapter.(c)  The commission by rule shall adopt criteria for beneficial uses to ensure that a beneficial use of recycled drill cuttings under this chapter is at least as protective of public health, public safety, and the environment as the use of an equivalent product made without recycled drill cuttings.
    Added by Acts 2017, 85th Leg., R.S., Ch. 184 (S.B. 1541), Sec. 2, eff. May 26, 2017.

    Sec. 123.002.  OWNERSHIP OF DRILL CUTTINGS TRANSFERRED FOR TREATMENT AND SUBSEQUENT BENEFICIAL USE.  Unless otherwise expressly provided by a contract, bill of sale, or other legally binding document:(1)  when drill cuttings are transferred to a permit holder who takes possession of the cuttings for the purpose of treating the cuttings for a subsequent beneficial use, the transferred material is considered to be the property of the permit holder until the permit holder transfers the cuttings or treated cuttings to another person for disposal or use; and(2)  when a permit holder who takes possession of drill cuttings for the purpose of treating the cuttings for a subsequent beneficial use transfers possession of the treated product or any treatment byproduct to another person for the purpose of subsequent disposal or beneficial use, the transferred product or byproduct is considered to be the property of the person to whom the material is transferred.
    Added by Acts 2015, 84th Leg., R.S., Ch. 351 (H.B. 1331), Sec. 3, eff. September 1, 2015.

    Sec. 123.003.  RESPONSIBILITY IN TORT.  A person who generates drill cuttings and transfers the drill cuttings to a permit holder with the contractual understanding that the drill cuttings will be used in connection with road building or another beneficial use is not liable in tort for a consequence of the subsequent use of the drill cuttings by the permit holder or by another person.
    Added by Acts 2015, 84th Leg., R.S., Ch. 351 (H.B. 1331), Sec. 3, eff. September 1, 2015.

    Sec. 123.004.  PERMIT COPY REQUIRED.  A permit holder who takes possession of drill cuttings from the person who generated the drill cuttings shall provide to the generator a copy of the holder's permit.
    Added by Acts 2015, 84th Leg., R.S., Ch. 351 (H.B. 1331), Sec. 3, eff. September 1, 2015.

    Sec. 123.005.  COMMISSION RULES, PERMITS, AND ORDERS FOR TREATMENT AND BENEFICIAL USE.  (a) The commission shall adopt rules to govern the treatment and beneficial use of drill cuttings.(b)  A rule adopted by the commission under this chapter or a permit or order issued by the commission regarding the treatment and beneficial use of drill cuttings must be at least as protective of public health, public safety, and the environment as a rule, permit, or order, respectively, adopted or issued by the commission regarding the disposal of drill cuttings.
    Added by Acts 2015, 84th Leg., R.S., Ch. 351 (H.B. 1331), Sec. 3, eff. September 1, 2015.Amended by: Acts 2017, 85th Leg., R.S., Ch. 184 (S.B. 1541), Sec. 3, eff. May 26, 2017.


                    
