     
                        
    HEALTH AND SAFETY CODE
    TITLE 7. MENTAL HEALTH AND INTELLECTUAL DISABILITY
    SUBTITLE E. SPECIAL PROVISIONS RELATING TO MENTAL ILLNESS AND MENTAL RETARDATION
    CHAPTER 613. KIDNEY DONATION BY WARD WITH MENTAL RETARDATION
    Sec. 613.001.  DEFINITION.  In this chapter, "ward with mental retardation" means a ward who is a person with mental retardation, as defined by Subtitle D.
    Added by Acts 1991, 72nd Leg., ch. 76, Sec. 1, eff. Sept. 1, 1991.

    Sec. 613.002.  COURT ORDER AUTHORIZING KIDNEY DONATION.  A district court may authorize the donation of a kidney of a ward with mental retardation to a father, mother, son, daughter, brother, or sister of the ward if:(1)  the guardian of the ward with mental retardation consents to the donation;(2)  the ward is 12 years of age or older;(3)  the ward assents to the kidney transplant;(4)  the ward has two kidneys;(5)  without the transplant the donee will soon die or suffer severe and progressive deterioration, and with the transplant the donee will probably benefit substantially;(6)  there are no medically preferable alternatives to a kidney transplant for the donee;(7)  the risks of the operation and the long-term risks to the ward are minimal;(8)  the ward will not likely suffer psychological harm;  and(9)  the transplant will promote the ward's best interests.
    Added by Acts 1991, 72nd Leg., ch. 76, Sec. 1, eff. Sept. 1, 1991.

    Sec. 613.003.  PETITION FOR COURT ORDER.  The guardian of the person of a ward with mental retardation may petition a district court having jurisdiction of the guardian for an order authorizing the ward to donate a kidney under Section 613.002.
    Added by Acts 1991, 72nd Leg., ch. 76, Sec. 1, eff. Sept. 1, 1991.

    Sec. 613.004.  COURT HEARING.  (a)  The court shall hold a hearing on the petition filed under Section 613.003.(b)  A party to the proceeding is entitled on request to a preferential setting for the hearing.(c)  The court shall appoint an attorney ad litem and a guardian ad litem to represent the interest of the ward with mental retardation.  Neither person appointed may be related to the ward within the second degree by consanguinity.(d)  The hearing must be adversary in order to secure a complete record, and the attorney ad litem shall advocate the ward's interest, if any, in not being a donor.(e)  The petitioner has the burden of establishing good cause for the kidney donation by establishing the prerequisites prescribed by Section 613.002.
    Added by Acts 1991, 72nd Leg., ch. 76, Sec. 1, eff. Sept. 1, 1991.

    Sec. 613.005.  INTERVIEW AND EVALUATION ORDER BY COURT.  (a)  Before the eighth day after the date of the hearing, the court shall interview the ward with mental retardation to determine if the ward assents to the donation.  The interview shall be conducted in chambers and out of the presence of the guardian.(b)  If the court considers it necessary, the court may order the performance of a determination of mental retardation, as provided by Section 593.005, to help the court evaluate the ward's capacity to agree to the donation.
    Added by Acts 1991, 72nd Leg., ch. 76, Sec. 1, eff. Sept. 1, 1991.  Amended by Acts 1993, 73rd Leg., ch. 60, Sec. 17, eff. Sept. 1, 1993.


                    
