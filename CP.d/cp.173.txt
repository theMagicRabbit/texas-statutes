        
                        
    CIVIL PRACTICE AND REMEDIES CODE
    TITLE 7. ALTERNATE METHODS OF DISPUTE RESOLUTION
    CHAPTER 173. ARBITRATION OF CONTROVERSIES BETWEEN MEMBERS OF CERTAIN NONPROFIT ENTITIES
    Sec. 173.001.  PURPOSE.  The purpose of this chapter is to abrogate the common law arbitration rule prohibiting specific enforcement of executory arbitration agreements.
    Added by Acts 1997, 75th Leg., ch. 165, Sec. 5.03, eff. Sept. 1, 1997.

    Sec. 173.002.  SCOPE OF CHAPTER.  This chapter applies only to the arbitration of a controversy between members of an association or corporation that is:(1)  exempt from the payment of federal income taxes under Section 501(a) of the Internal Revenue Code of 1986 by being listed as an exempt organization under Section 501(c) of the code;  or(2)  incorporated under the Texas Non-Profit Corporation Act (Article 1396-1.01 et seq., Vernon's Texas Civil Statutes).
    Added by Acts 1997, 75th Leg., ch. 165, Sec. 5.03, eff. Sept. 1, 1997.

    Sec. 173.003.  AGREEMENT OR BYLAW PROVISION VALID.  (a)  A written agreement to submit a controversy to arbitration at common law is valid and enforceable if the agreement is to arbitrate a controversy that arises between the parties after the date of the agreement.(b)  A party may revoke the agreement only on a ground that exists at law or in equity for the revocation of a contract.(c)  A provision in the bylaws of a nonprofit corporation incorporated under the Texas Non-Profit Corporation Act (Article 1396-1.01 et seq., Vernon's Texas Civil Statutes) that requires a member of the corporation to arbitrate at common law a controversy that subsequently arises between members is a valid, enforceable, and irrevocable agreement by a member of the corporation to arbitrate the controversy.
    Added by Acts 1997, 75th Leg., ch. 165, Sec. 5.03, eff. Sept. 1, 1997.

    Sec. 173.004.  COMMON LAW PRESERVED.  This chapter is cumulative of other law relating to common law arbitration.  Except as specifically provided by this chapter, this chapter does not abrogate or repeal that other law.
    Added by Acts 1997, 75th Leg., ch. 165, Sec. 5.03, eff. Sept. 1, 1997.


                    
