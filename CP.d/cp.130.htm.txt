     
                        
    CIVIL PRACTICE AND REMEDIES CODE
    TITLE 6. MISCELLANEOUS PROVISIONS
    CHAPTER 130. INDEMNIFICATION IN CERTAIN CONSTRUCTION CONTRACTS
    Sec. 130.001.  DEFINITION.  In this chapter "construction contract" means a contract or agreement made and entered into by an owner, contractor, subcontractor, registered architect, licensed engineer, or supplier concerning the design, construction, alteration, repair, or maintenance of a building, structure, appurtenance, road, highway, bridge, dam, levee, or other improvement to or on real property, including moving, demolition, and excavation connected with the real property.
    Added by Acts 1987, 70th Leg., ch. 167, Sec. 3.14(a), eff. Sept. 1, 1987.  Amended by Acts 2001, 77th Leg., ch. 351, Sec. 2, eff. Sept. 1, 2001.

    Sec. 130.002.  COVENANT OR PROMISE VOID AND UNENFORCEABLE.  (a)  A covenant or promise in, in connection with, or collateral to a construction contract is void and unenforceable if the covenant or promise provides for a contractor who is to perform the work that is the subject of the construction contract to indemnify or hold harmless a registered architect, licensed engineer or an agent, servant, or employee of a registered architect or licensed engineer from liability for damage that:(1)  is caused by or results from:(A)  defects in plans, designs, or specifications prepared, approved, or used by the architect or engineer;  or(B)  negligence of the architect or engineer in the rendition or conduct of professional duties called for or arising out of the construction contract and the plans, designs, or specifications that are a part of the construction contract;  and(2)  arises from:(A)  personal injury or death;(B)  property injury;  or(C)  any other expense that arises from personal injury, death, or property injury.(b)  A covenant or promise in, in connection with, or collateral to a construction contract other than a contract for a single family or multifamily residence is void and unenforceable if the covenant or promise provides for a registered architect or licensed engineer whose engineering or architectural design services are the subject of the construction contract to indemnify or hold harmless an owner or owner's agent or employee from liability for damage that is caused by or results from the negligence of an owner or an owner's agent or employee.
    Added by Acts 1987, 70th Leg., ch. 167, Sec. 3.14(a), eff. Sept. 1, 1987.  Amended by Acts 2001, 77th Leg., ch. 351, Sec. 3, eff. Sept. 1, 2001.

    Sec. 130.003.  INSURANCE CONTRACT;  WORKERS' COMPENSATION.  This chapter does not apply to:(1)  an insurance contract;  or(2)  a workers' compensation agreement.
    Added by Acts 1987, 70th Leg., ch. 167, Sec. 3.14(a), eff. Sept. 1, 1987.

    Sec. 130.004.  OWNER OF INTEREST IN REAL PROPERTY.  (a)  Except as provided by Section 130.002(b), this chapter does not apply to an owner of an interest in real property or persons employed solely by that owner.(b)  Except as provided by Section 130.002(b), this chapter does not prohibit or make void or unenforceable a covenant or promise to:(1)  indemnify or hold harmless an owner of an interest in real property and persons employed solely by that owner;  or(2)  allocate, release, liquidate, limit, or exclude liability in connection with a construction contract between an owner or other person for whom a construction contract is being performed and a registered architect or licensed engineer.
    Added by Acts 1987, 70th Leg., ch. 167, Sec. 3.14(a), eff. Sept. 1, 1987.  Amended by Acts 2001, 77th Leg., ch. 351, Sec. 4, eff. Sept. 1, 2001.

    Sec. 130.005.  APPLICATION OF CHAPTER.  This chapter does not apply to a contract or agreement in which an architect or engineer or an agent, servant, or employee of an architect or engineer is indemnified from liability for:(1)  negligent acts other than those described by this chapter;  or(2)  negligent acts of the contractor, any subcontractor, any person directly or indirectly employed by the contractor or a subcontractor, or any person for whose acts the contractor or a subcontractor may be liable.
    Added by Acts 1987, 70th Leg., ch. 167, Sec. 3.14(a), eff. Sept. 1, 1987.


                    
