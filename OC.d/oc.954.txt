       
                        
    OCCUPATIONS CODE
    TITLE 5. REGULATION OF FINANCIAL AND LEGAL SERVICES
    SUBTITLE B. LEGAL SERVICES
    CHAPTER 954. PETROLEUM AND MINERAL LAND SERVICES
    Sec. 954.001.  EXCEPTION TO PRACTICE OF LAW.  For the purposes of the definition in Section 81.101, Government Code, the "practice of law" does not include acts relating to the lease, purchase, sale, or transfer of a mineral or mining interest in real property or an easement or other interest associated with a mineral or mining interest in real property if:(1)  the acts are performed by a person who does not hold the person out as an attorney licensed to practice law in this state or in another jurisdiction;  and(2)  the person is not a licensed attorney.
    Added by Acts 2003, 78th Leg., ch. 696, Sec. 1, eff. June 20, 2003.Renumbered from Occupations Code, Section 953.001 by Acts 2005, 79th Leg., Ch. 728 (H.B. 2018), Sec. 23.001(75), eff. September 1, 2005.


                    
