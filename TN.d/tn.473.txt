        
                        
    TRANSPORTATION CODE
    TITLE 6. ROADWAYS
    SUBTITLE Z. MISCELLANEOUS ROADWAY PROVISIONS
    This Chapter 473, consisting of Secs. 473.001 to 473.004, was added by Acts 2019, 86th Leg., R.S., Ch. 382 (H.B. 2899), Sec. 1.
    See also another Chapter 473, consisting of Secs. 473.001 to 473.004, as added by Acts 2019, 86th Leg., R.S., Ch. 467 (H.B. 4170), Sec. 20.001.
    CHAPTER 473.  RESPONSIBILITY FOR DEFECTS IN PLANS AND SPECIFICATIONS
    Sec. 473.001.  DEFINITIONS.  In this chapter:(1)  "Contract" means a contract for the construction or repair of a road or highway of any number of lanes, with or without grade separation, owned or operated by a governmental entity and any improvement, extension, or expansion to that road or highway, including:(A)  an improvement to relieve traffic congestion and promote safety;(B)  a bridge, tunnel, overpass, underpass, interchange, service road ramp, entrance plaza, approach, or tollhouse; and(C)  a parking area or structure, rest stop, park, or other improvement or amenity the governmental entity considers necessary, useful, or beneficial for the operation of a road or highway.(2)  "Contractor" means a person who is required to perform work under a contract.(3)  "Governmental entity" means:(A)  the Texas Department of Transportation; or(B)  any political subdivision of the state that is acting under Chapter 284, 366, 370, or 431.(4)  "Project specifications" means plans, reports, designs, or specifications prepared by a governmental entity or by a third party retained by a governmental entity under a separate contract.
    Added by Acts 2019, 86th Leg., R.S., Ch. 382 (H.B. 2899), Sec. 1, eff. June 2, 2019.

    Sec. 473.002.  APPLICABILITY.  This chapter applies to a governmental entity authorized by state law to make a contract and to any contractor with whom a governmental entity enters into a contract.
    Added by Acts 2019, 86th Leg., R.S., Ch. 382 (H.B. 2899), Sec. 1, eff. June 2, 2019.

    Sec. 473.003.  LIMITATION ON CONTRACTOR'S RESPONSIBILITY FOR CERTAIN DEFECTS.  (a)  A contractor who enters into a contract with a governmental entity is not civilly liable or otherwise responsible for the accuracy, adequacy, sufficiency, suitability, or feasibility of any project specifications and is not liable for any damage to the extent caused by:(1)  a defect in those project specifications; or(2)  the errors, omissions, or negligent acts of a governmental entity, or of a third party retained by a governmental entity under a separate contract, in the rendition or conduct of professional duties arising out of or related to the project specifications.(b)  A covenant or promise contained in a contract governed by this chapter is void and unenforceable to the extent that the covenant or promise conflicts with Subsection (a).(c)  This section does not apply to a consultant retained in a separate contract by a governmental entity to expressly monitor the compliance with project specifications by another contractor with whom the governmental entity has entered into a contract.(d)  This section does not relieve a contractor from the contractor's obligations or liability under a contract with a governmental entity.
    Added by Acts 2019, 86th Leg., R.S., Ch. 382 (H.B. 2899), Sec. 1, eff. June 2, 2019.

    Sec. 473.004.  ENGINEER'S OR ARCHITECT'S STANDARD OF CARE.  A governmental entity may not require that engineering or architectural services be performed to a level of professional skill and care beyond the level that would be provided by an ordinarily prudent engineer or architect with the same professional license and under the same or similar circumstances in a contract:(1)  for engineering or architectural services; or(2)  that contains engineering or architectural services as a component part.
    Added by Acts 2019, 86th Leg., R.S., Ch. 382 (H.B. 2899), Sec. 1, eff. June 2, 2019.


                    
