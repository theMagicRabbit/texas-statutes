     
                        
    TRANSPORTATION CODE
    TITLE 7. VEHICLES AND TRAFFIC
    SUBTITLE M.  DEPARTMENT OF MOTOR VEHICLES
    CHAPTER 1005.  STANDARDS OF CONDUCT
    Sec. 1005.001.  APPLICATION OF LAW RELATING TO ETHICAL CONDUCT.  The board, the executive director, and each employee or agent of the department is subject to the code of ethics and the standard of conduct imposed by Chapter 572, Government Code, and any other law regulating the ethical conduct of state officers and employees.
    Added by Acts 2009, 81st Leg., R.S., Ch. 933 (H.B. 3097), Sec. 1.01, eff. September 1, 2009.


                    
