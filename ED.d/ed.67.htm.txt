        
                        
    EDUCATION CODE
    TITLE 3. HIGHER EDUCATION
    SUBTITLE C. THE UNIVERSITY OF TEXAS SYSTEM
    CHAPTER 67. THE UNIVERSITY OF TEXAS AT AUSTIN
    SUBCHAPTER A. GENERAL PROVISIONS
    Sec. 67.01.  DEFINITIONS.  In this chapter:(1)  "University" means the University of Texas at Austin.(2)  "Board" means the board of regents of The University of Texas System.
    Acts 1971, 62nd Leg., p. 3159, ch. 1024, art. 1, Sec. 1, eff. Sept. 1, 1971.

    Sec. 67.02.  THE UNIVERSITY OF TEXAS AT AUSTIN.  The University of Texas at Austin is a coeducational institution of higher education within The University of Texas System.  It is under the management and control of the board of regents of The University of Texas System.
    Acts 1971, 62nd Leg., p. 3160, ch. 1024, art. 1, Sec. 1, eff. Sept. 1, 1971.

    Sec. 67.03.  CAMPUS PEACE OFFICERS: CONCURRENT JURISDICTION.  (a)  Campus peace officers commissioned by the university have the same jurisdiction, powers, privileges, and immunities as provided by Section 51.203.(b)  Subsection (a) does not in any manner limit or reduce the jurisdiction, powers, privileges, and immunities provided by law for a law enforcement agency of the state or a political subdivision of the state, including the City of Austin police department, with territorial jurisdiction that includes all or part of the university campus.  The law enforcement agency retains the autonomous authority to deploy agency personnel on university property and in university facilities in any manner consistent with the jurisdiction, powers, privileges, and immunities of the agency.
    Added by Acts 2005, 79th Leg., Ch. 492 (H.B. 479), Sec. 1, eff. June 17, 2005.

    SUBCHAPTER B. POWERS AND DUTIES OF BOARD
    Sec. 67.22.  MILITARY TRAINING.  No student of the university shall ever be required to take a military training course as a condition for entrance into the university or for graduation from the university.
    Acts 1971, 62nd Leg., p. 3160, ch. 1024, art. 1, Sec. 1, eff. Sept. 1, 1971.

    Sec. 67.23.  TEXAS MEMORIAL MUSEUM.  The board has the management and control of the Texas Memorial Museum.  It shall be maintained as a museum and shall be an integral part of The University of Texas at Austin.
    Acts 1971, 62nd Leg., p. 3160, ch. 1024, art. 1, Sec. 1, eff. Sept. 1, 1971.

    Sec. 67.24.  RESEARCH AND EXPERIMENTATION FOR TEXAS DEPARTMENT OF TRANSPORTATION.  (a)  The department may contract with the university for the university to conduct research relating to transportation, including the economics, planning, design, construction, maintenance, or operation of transportation facilities.(b)  An agreement entered into under this section is not subject to Chapter 771, Government Code.(c)  The comptroller may draw proper warrants in favor of the university based on vouchers or claims submitted by the university through the department covering reasonable fees and charges for services rendered by members of the staff of the university system to the department and for equipment and materials necessary for research and experimentation under a contract entered into under this section.(d)  The comptroller shall pay warrants issued under this section against any funds appropriated by the legislature to the department. The payments made to the university shall be credited and deposited to local institutional funds under its control.(e)  In this section:(1)  "Department" means the Texas Department of Transportation.(2)  "Transportation facilities" means highways, turnpikes, airports, railroads, including high-speed railroads, bicycle and pedestrian facilities, waterways, pipelines, electric utility facilities, communication lines and facilities, public transportation facilities, port facilities, and facilities appurtenant to other transportation facilities.
    Acts 1971, 62nd Leg., p. 3161, ch. 1024, art. 1, Sec. 1, eff. Sept. 1, 1971.  Amended by Acts 1995, 74th Leg., ch. 165, Sec. 22(29), eff. Sept. 1, 1995;  Acts 1997, 75th Leg., ch. 382, Sec. 1, eff. May 28, 1997;  Acts 1997, 75th Leg., ch. 1423, Sec. 5.20, eff. Sept. 1, 1997.

    Sec. 67.25.  SESQUICENTENNIAL MUSEUM.  The University of Texas at Austin may contract with the Texas Sesquicentennial Museum Board to operate the Texas Sesquicentennial Museum.
    Added by Acts 1981, 67th Leg., p. 2450, ch. 630, Sec. 2, eff. Sept. 1, 1981.

    Sec. 67.26.  UNIVERSITY INTERSCHOLASTIC LEAGUE;  VENUE FOR SUITS.  Venue for suits brought against the University Interscholastic League or for suits involving the interpretation or enforcement of the rules or regulations of the University Interscholastic League shall be in Travis County, Texas.  When the litigation involves a school district located within Travis County, it shall be heard by a visiting judge.
    Added by Acts 1986, 69th Leg., 3rd C.S., ch. 12, Sec. 1, eff. Oct. 2, 1986.  Amended by Acts 1993, 73rd Leg., ch. 107, Sec. 2.01, eff. Aug. 30, 1993.

    Sec. 67.27.  RESEARCH FACILITIES ON BALCONES TRACT.  (a)  Under the general authority of Section 65.39 of this code, the board is specifically authorized to lease vacant land on the Balcones Tract to a corporation created under the Texas Non-Profit Corporation Act (Article 1396-1.01, Vernon's Texas Civil Statutes) for the scientific and educational purpose of assisting in the provision of research facilities for The University of Texas System and may include in the terms of the lease agreement provisions for the lease back of the land along with any facilities that may be constructed thereon by the nonprofit corporation.(b)  Insofar as permissible under federal laws and regulations, the board may obligate for payment to the nonprofit corporation under the terms of the lease agreement appropriate portions of federal or private funds to be received under the terms of federal or private research grants and contracts.(c)  The terms of the lease agreement shall provide for a termination date not more than 30 years from the date the lease agreement is entered into and shall further provide that ownership of facilities constructed on the leasehold estate shall revert to the board upon the expiration of the lease term.
    Added by Acts 1987, 70th Leg., ch. 646, Sec. 1, eff. Aug. 31, 1987.

    SUBCHAPTER C. THE UNIVERSITY OF TEXAS MCDONALD OBSERVATORY AT MOUNT LOCKE
    Sec. 67.51.  UNIT OF UNIVERSITY.  The University of Texas McDonald Observatory at Mount Locke is a part of and under the direction and control of The University of Texas at Austin.
    Acts 1971, 62nd Leg., p. 3161, ch. 1024, art. 1, Sec. 1, eff. Sept. 1, 1971.

    Sec. 67.52.  PROGRAMS.  The observatory shall conduct basic research in astronomy, along with optical and radio astronomy research, toward the establishment of a highly developed astronomy and space-science program, including the acquisition and support of the technical and maintenance staffs and facilities essential to the operation of an observatory of the first class, and may assist in the conduct of a comprehensive instructional program in astronomy and space science.
    Added by Acts 1971, 62nd Leg., p. 3361, ch. 1024, art. 2, Sec. 40, eff. Sept. 1, 1971.

    Sec. 67.53.  VISITOR CENTER.  The board may negotiate and contract with the Texas Department of Transportation and any other agency, department, or political subdivision of the state or any individual for the construction, maintenance, and operation of a visitor center and related facilities at McDonald Observatory at Mount Locke.
    Added by Acts 1975, 64th Leg., p. 370, ch. 161, Sec. 1, eff. May 8, 1975.  Amended by Acts 1995, 74th Leg., ch. 165, Sec. 22(30), eff. Sept. 1, 1995.

    SUBCHAPTER D. THE UNIVERSITY OF TEXAS MARINE SCIENCE INSTITUTE
    Sec. 67.61.  UNIT OF UNIVERSITY.  The University of Texas Marine Science Institute is a part of and under the direction and control of The University of Texas at Austin.
    Acts 1971, 62nd Leg., p. 3161, ch. 1024, art. 1, Sec. 1, eff. Sept. 1, 1971.  Amended by Acts 1973, 63rd Leg., p. 481, ch. 208, Sec. 1, eff. May 26, 1973.

    Sec. 67.62.  PROGRAMS, COURSES, FACILITIES.  The institute shall conduct a comprehensive instructional program in marine science, resources, and engineering at the graduate level and offer undergraduate courses for those students interested in the marine environment, and perform basic and applied research in the marine environment;  and may provide shore-based facilities, including, but not limited to, laboratories, boats, classrooms, dormitories, and a cafeteria for faculty and students who are engaged in studies of the marine environment.
    Acts 1971, 62nd Leg., p. 3361, ch. 1024, art. 2, Sec. 39, eff. Sept. 1, 1971.  Amended by Acts 1973, 63rd Leg., p. 481, ch. 208, Sec. 1, eff. May 26, 1973.

    SUBCHAPTER E.  THE UNIVERSITY OF TEXAS BUREAU OF ECONOMIC GEOLOGY
    Sec. 67.71.  DEFINITION.  In this subchapter, "bureau" means The University of Texas Bureau of Economic Geology.
    Added by Acts 2017, 85th Leg., R.S., Ch. 871 (H.B. 2819), Sec. 1, eff. September 1, 2017.


                    
