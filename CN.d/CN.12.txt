     
                        
    THE TEXAS CONSTITUTION
    ARTICLE 12. PRIVATE CORPORATIONS
    Sec. 1.  CREATION OF PRIVATE CORPORATIONS BY GENERAL LAWS ONLY.  No private corporation shall be created except by general laws. Sec. 2.  GENERAL LAWS FOR CREATION OF PRIVATE CORPORATIONS AND PROTECTION OF PUBLIC AND STOCKHOLDERS.  General laws shall be enacted providing for the creation of private corporations, and shall therein provide fully for the adequate protection of the public and of the individual stockholders. Sec. 3.  (Repealed Aug. 5, 1969.) Sec. 4.  (Repealed Aug. 5, 1969.) Sec. 5.  (Repealed Aug. 5, 1969.) Sec. 6.  (Repealed Nov. 2, 1993.) Sec. 7.  (Repealed Aug. 5, 1969.) 
                    
