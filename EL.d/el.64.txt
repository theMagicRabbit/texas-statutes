     
                        
    ELECTION CODE
    TITLE 6. CONDUCT OF ELECTIONS
    CHAPTER 64. VOTING PROCEDURES
    SUBCHAPTER A. VOTING GENERALLY
    Sec. 64.001.  VOTER TO SELECT AND PREPARE BALLOT.  (a)  After a voter is accepted for voting, the voter shall select a ballot, go to a voting station, and prepare the ballot.(b)  A voter who executes an affidavit in accordance with Section 63.011 shall select a provisional ballot.
    Acts 1985, 69th Leg., ch. 211, Sec. 1, eff. Jan. 1, 1986.  Amended by Acts 1997, 75th Leg., ch. 1078, Sec. 11, eff. Sept. 1, 1997;  Acts 2003, 78th Leg., ch. 1315, Sec. 30, eff. Jan. 1, 2004.

    Sec. 64.002.  OCCUPANCY OF VOTING STATION.  (a)  Except as otherwise provided by this code, only one person at a time may occupy a voting station.(b)  A child under 18 years of age may accompany the child's parent to a voting station.
    Acts 1985, 69th Leg., ch. 211, Sec. 1, eff. Jan. 1, 1986.  Amended by Acts 1993, 73rd Leg., ch. 728, Sec. 16, eff. Sept. 1, 1993;  Acts 1997, 75th Leg., ch. 864, Sec. 61, eff. Sept. 1, 1997.

    Sec. 64.003.  MARKING THE BALLOT FOR CANDIDATE ON BALLOT.  A vote for a particular candidate whose name is on the ballot must be indicated by placing an "X" or other mark that clearly shows the voter's intent in the square beside the name of the candidate for whom the voter desires to vote.
    Acts 1985, 69th Leg., ch. 211, Sec. 1, eff. Jan. 1, 1986.

    Sec. 64.005.  MARKING THE BALLOT FOR WRITE-IN CANDIDATE.  In an election in which write-in voting is permitted, a vote for a candidate who is not on the ballot must be indicated by writing the candidate's name in the appropriate place provided on the ballot.
    Acts 1985, 69th Leg., ch. 211, Sec. 1, eff. Jan. 1, 1986.

    Sec. 64.006.  MARKING THE BALLOT FOR MEASURE.  A vote on a particular measure must be indicated by placing an "X" or other mark that clearly shows the voter's intent in the appropriate square that is beside the proposition and that indicates the way the voter desires to vote on the measure.
    Acts 1985, 69th Leg., ch. 211, Sec. 1, eff. Jan. 1, 1986.

    Sec. 64.007.  SPOILED BALLOT.  (a)  If a voter mismarks, damages, or otherwise spoils the ballot in the process of voting, the voter is entitled to receive a new ballot by returning the spoiled ballot to an election officer.(b)  A voter is not entitled to receive more than three ballots.(c)  An election officer shall maintain a register of spoiled ballots at the polling place.  An election officer shall enter on the register the name of each voter who returns a spoiled ballot and the spoiled ballot's number.(d)  After making the appropriate entry on the register, the election officer shall deposit the spoiled ballot in ballot box no. 4.
    Acts 1985, 69th Leg., ch. 211, Sec. 1, eff. Jan. 1, 1986.

    Sec. 64.008.  DEPOSITING BALLOT.  (a)  Except as provided by Subsection (b), after a voter has marked the ballot, the voter shall fold the ballot to conceal the way it is marked but to expose the presiding judge's signature, and shall deposit it in the ballot box used for the deposit of marked ballots.(b)  After a voter has marked a provisional ballot, the voter shall enclose the ballot in the envelope on which the voter's executed affidavit is printed.  The person shall seal the envelope and deposit it in a box available for the deposit of provisional ballots.(c)  At the time a person casts a provisional ballot under Subsection (b), an election officer shall give the person written information describing how the person may use the free access system established under Section 65.059 to obtain information on the disposition of the person's vote.
    Acts 1985, 69th Leg., ch. 211, Sec. 1, eff. Jan. 1, 1986.  Amended by Acts 2003, 78th Leg., ch. 1315, Sec. 31, eff. Jan. 1, 2004.

    Sec. 64.009.  VOTER UNABLE TO ENTER POLLING PLACE.  (a)  If a voter is physically unable to enter the polling place without personal assistance or likelihood of injuring the voter's health, on the voter's request, an election officer shall deliver a ballot to the voter at the polling place entrance or curb.(b)  The regular voting procedures may be modified by the election officer to the extent necessary to conduct voting under this section.(c)  After the voter is accepted for voting, the voter shall mark the ballot and give it to the election officer who shall deposit it in the ballot box.(d)  On the voter's request, a person accompanying the voter shall be permitted to select the voter's ballot and deposit the ballot in the ballot box.
    Acts 1985, 69th Leg., ch. 211, Sec. 1, eff. Jan. 1, 1986.  Amended by Acts 1997, 75th Leg., ch. 864, Sec. 62, eff. Sept. 1, 1997.

    Sec. 64.010.  UNLAWFULLY PERMITTING OR PREVENTING DEPOSIT OF BALLOT.  (a)  An election officer commits an offense if the officer:(1)  permits a person to deposit in the ballot box a ballot that the officer knows was not provided at the polling place to the voter who is depositing the ballot or for whom the deposit is made;  or(2)  prevents the deposit in the ballot box of a marked and properly folded ballot that was provided at the polling place to the voter who is depositing it or for whom the deposit is attempted.(b)  An offense under this section is a Class B misdemeanor.
    Acts 1985, 69th Leg., ch. 211, Sec. 1, eff. Jan. 1, 1986.

    Sec. 64.011.  UNLAWFULLY DEPOSITING BALLOT.  (a)  A person commits an offense if the person deposits or attempts to deposit in a ballot box a ballot that was not provided to the person who is depositing the ballot or for whom the deposit is made or attempted.(b)  An offense under this section is a Class A misdemeanor unless the person is convicted of an attempt.  In that case, the offense is a Class B misdemeanor.
    Acts 1985, 69th Leg., ch. 211, Sec. 1, eff. Jan. 1, 1986.

    Sec. 64.012.  ILLEGAL VOTING.  (a)  A person commits an offense if the person:(1)  votes or attempts to vote in an election in which the person knows the person is not eligible to vote;(2)  knowingly votes or attempts to vote more than once in an election;(3)  knowingly votes or attempts to vote a ballot belonging to another person, or by impersonating another person; or(4)  knowingly marks or attempts to mark any portion of another person's ballot without the consent of that person, or without specific direction from that person how to mark the ballot.(b)  An offense under this section is a felony of the second degree unless the person is convicted of an attempt.  In that case, the offense is a state jail felony.
    Acts 1985, 69th Leg., ch. 211, Sec. 1, eff. Jan. 1, 1986.  Amended by Acts 1997, 75th Leg., ch. 864, Sec. 63, eff. Sept. 1, 1997;  Acts 2003, 78th Leg., ch. 393, Sec. 3, eff. Sept. 1, 2003.Amended by: Acts 2011, 82nd Leg., R.S., Ch. 123 (S.B. 14), Sec. 16, eff. January 1, 2012.Acts 2017, 85th Leg., 1st C.S., Ch. 1 (S.B. 5), Sec. 1, eff. December 1, 2017.

    SUBCHAPTER B. ASSISTING VOTER
    Sec. 64.031.  ELIGIBILITY FOR ASSISTANCE.  A voter is eligible to receive assistance in marking the ballot, as provided by this subchapter, if the voter cannot prepare the ballot because of:(1)  a physical disability that renders the voter unable to write or see;  or(2)  an inability to read the language in which the ballot is written.
    Acts 1985, 69th Leg., ch. 211, Sec. 1, eff. Jan. 1, 1986.

    Sec. 64.032.  PERSONS PROVIDING ASSISTANCE.  (a)  Except as provided by Subsection (c), on a voter's request for assistance in marking the ballot, two election officers shall provide the assistance.(b)  If a voter is assisted by election officers in the general election for state and county officers, each officer must be aligned with a different political party unless there are not two or more election officers serving the polling place who are aligned with different parties.(c)  On the voter's request, the voter may be assisted by any person selected by the voter other than the voter's employer, an agent of the voter's employer, or an officer or agent of a labor union to which the voter belongs.(d)  If assistance is provided by a person of the voter's choice, an election officer shall enter the person's name and address on the poll list beside the voter's name.
    Acts 1985, 69th Leg., ch. 211, Sec. 1, eff. Jan. 1, 1986.

    Sec. 64.0321.  DEFINITION.  For purposes of this subchapter and Sections 85.035 and 86.010, assisting a voter includes the following conduct by a person other than the voter that occurs while the person is in the presence of the voter's ballot or carrier envelope:(1)  reading the ballot to the voter;(2)  directing the voter to read the ballot;(3)  marking the voter's ballot;  or(4)  directing the voter to mark the ballot.
    Added by Acts 2003, 78th Leg., ch. 393, Sec. 4, eff. Sept. 1, 2003;  Acts 2003, 78th Leg., ch. 640, Sec. 1, eff. June 20, 2003.

    Sec. 64.033.  READING BALLOT TO VOTER.  (a)  If a voter is assisted by election officers, one of them shall read the entire ballot to the voter unless the voter tells the officer that the voter desires to vote only on certain offices or measures.  In that case, the officer shall read those items on the ballot specified by the voter.(b)  If a voter is assisted by a person of the voter's choice, an election officer shall ask the voter being assisted whether the voter wants the entire ballot read to the voter. If so, the officer shall instruct the person assisting the voter to read the entire ballot to the voter.
    Acts 1985, 69th Leg., ch. 211, Sec. 1, eff. Jan. 1, 1986.  Amended by Acts 1997, 75th Leg., ch. 864, Sec. 64, eff. Sept. 1, 1997.

    Sec. 64.034.  OATH.  A person selected to provide assistance to a voter must take the following oath, administered by an election officer at the polling place, before providing assistance:"I swear (or affirm) that I will not suggest, by word, sign, or gesture, how the voter should vote; I will confine my assistance to answering the voter's questions, to stating propositions on the ballot, and to naming candidates and, if listed, their political parties; I will prepare the voter's ballot as the voter directs; and I am not the voter's employer, an agent of the voter's employer, or an officer or agent of a labor union to which the voter belongs."
    Acts 1985, 69th Leg., ch. 211, Sec. 1, eff. Jan. 1, 1986.Amended by: Acts 2013, 83rd Leg., R.S., Ch. 358 (H.B. 2475), Sec. 1, eff. September 1, 2013.

    Sec. 64.035.  DEPOSITING BALLOT.  After assistance has been provided in marking a ballot, the ballot shall be folded and deposited in the ballot box by the voter or, on the voter's request, by the person assisting the voter.
    Acts 1985, 69th Leg., ch. 211, Sec. 1, eff. Jan. 1, 1986.

    Sec. 64.036.  UNLAWFUL ASSISTANCE.  (a)  A person commits an offense if the person knowingly:(1)  provides assistance to a voter who is not eligible for assistance;(2)  while assisting a voter prepares the voter's ballot in a way other than the way the voter directs or without direction from the voter;(3)  while assisting a voter suggests by word, sign, or gesture how the voter should vote;  or(4)  provides assistance to a voter who has not requested assistance or selected the person to assist the voter.(b)  A person commits an offense if the person knowingly assists a voter in violation of Section 64.032(c).(c)  An election officer commits an offense if the officer knowingly permits a person to provide assistance:(1)  to a voter who is not eligible for assistance;  or(2)  in violation of Section 64.032(c).(d)  An offense under this section is a Class A misdemeanor.
    Acts 1985, 69th Leg., ch. 211, Sec. 1, eff. Jan. 1, 1986.  Amended by Acts 2003, 78th Leg., ch. 393, Sec. 5, eff. Sept. 1, 2003.

    Sec. 64.037.  UNAUTHORIZED ASSISTANCE VOIDS BALLOT.  If assistance is provided to a voter who is not eligible for assistance, the voter's ballot may not be counted.
    Acts 1985, 69th Leg., ch. 211, Sec. 1, eff. Jan. 1, 1986.


                    
