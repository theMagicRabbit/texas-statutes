        
                        
    HUMAN RESOURCES CODE
    TITLE 2. HUMAN SERVICES AND PROTECTIVE SERVICES IN GENERAL
    SUBTITLE E. SERVICES FOR FAMILIES
    CHAPTER 53.  PREVENTIVE SERVICES FOR VETERANS AND MILITARY FAMILIES
    Sec. 53.001.  DEFINITIONS.  In this chapter:(1)  "Department" means the Department of Family and Protective Services.(2)  "Veteran" means a person who has served in:(A)  the army, navy, air force, coast guard, or marine corps of the United States;(B)  the state military forces as defined by Section 431.001, Government Code; or(C)  an auxiliary service of one of those branches of the armed forces.
    Added by Acts 2015, 84th Leg., R.S., Ch. 324 (H.B. 19), Sec. 1, eff. June 4, 2015.

    Sec. 53.002.  VETERANS AND MILITARY FAMILIES PREVENTIVE SERVICES PROGRAM.  (a)  The department shall develop and implement a preventive services program to serve veterans and military families who have committed or experienced or who are at a high risk of:(1)  family violence; or(2)  abuse or neglect.(b)  The program must:(1)  be designed to coordinate with community-based organizations to provide prevention services;(2)  include a prevention component and an early intervention component;(3)  include collaboration with services for child welfare, services for early childhood education, and other child and family services programs; and(4)  coordinate with the community collaboration initiative developed under Subchapter I, Chapter 434, Government Code, and committees formed by local communities as part of that initiative.(c)  The program must be established initially as a pilot program in areas of the state in which the department considers the implementation practicable.  The department shall evaluate the outcomes of the pilot program and ensure that the program is producing positive results before implementing the program throughout the state.(d)  The department shall evaluate the program and prepare an annual report on the outcomes of the program.  The department shall publish the report on the department's Internet website.
    Added by Acts 2015, 84th Leg., R.S., Ch. 324 (H.B. 19), Sec. 1, eff. June 4, 2015.


                    
