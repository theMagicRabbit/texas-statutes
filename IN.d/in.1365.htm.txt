        
                        
    INSURANCE CODE
    TITLE 8. HEALTH INSURANCE AND OTHER HEALTH COVERAGES
    SUBTITLE E. BENEFITS PAYABLE UNDER HEALTH COVERAGES
    CHAPTER 1365. LOSS OR IMPAIRMENT OF SPEECH OR HEARING
    Sec. 1365.001.  APPLICABILITY OF CHAPTER.  This chapter applies only to a group health benefit plan that provides hospital and medical coverage on an expense-incurred, service, or prepaid basis, including a group policy, contract, or plan that is offered in this state by:(1)  an insurer;(2)  a group hospital service corporation operating under Chapter 842;  or(3)  a health maintenance organization operating under Chapter 843.
    Added by Acts 2003, 78th Leg., ch. 1274, Sec. 3, eff. April 1, 2005.

    Sec. 1365.002.  APPLICABILITY OF GENERAL PROVISIONS OF OTHER LAW.  The provisions of Chapter 1201, including provisions relating to the applicability, purpose, and enforcement of that chapter, construction of policies under that chapter, rulemaking under that chapter, and definitions of terms applicable in that chapter, apply to this chapter.
    Added by Acts 2003, 78th Leg., ch. 1274, Sec. 3, eff. April 1, 2005.

    Sec. 1365.003.  OFFER OF COVERAGE REQUIRED.  (a)  A group health benefit plan issuer shall offer and make available under the plan coverage for the necessary care and treatment of loss or impairment of speech or hearing.(b)  Coverage required under this section:(1)  may not be less favorable than coverage for physical illness generally under the plan;  and(2)  must be subject to the same durational limits, dollar limits, deductibles, and coinsurance factors as coverage for physical illness generally under the plan.
    Added by Acts 2003, 78th Leg., ch. 1274, Sec. 3, eff. April 1, 2005.

    Sec. 1365.004.  RIGHT TO REJECT COVERAGE OR SELECT ALTERNATIVE BENEFITS.  An offer of coverage required under Section 1365.003 is subject to the right of the group contract holder to reject the coverage or to select an alternative level of benefits that is offered by or negotiated with the group health benefit plan issuer.
    Added by Acts 2003, 78th Leg., ch. 1274, Sec. 3, eff. April 1, 2005.Amended by: Acts 2007, 80th Leg., R.S., Ch. 730 (H.B. 2636), Sec. 3B.028, eff. September 1, 2007.Acts 2007, 80th Leg., R.S., Ch. 921 (H.B. 3167), Sec. 9.028, eff. September 1, 2007.


                    
