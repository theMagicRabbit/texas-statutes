       
                        
    INSURANCE CODE
    TITLE 8. HEALTH INSURANCE AND OTHER HEALTH COVERAGES
    SUBTITLE E. BENEFITS PAYABLE UNDER HEALTH COVERAGES
    CHAPTER 1363. CERTAIN TESTS FOR DETECTION OF COLORECTAL CANCER
    Sec. 1363.001.  APPLICABILITY OF CHAPTER.  This chapter applies only to a health benefit plan that:(1)  provides benefits for medical or surgical expenses incurred as a result of a health condition, accident, or sickness, including:(A)  an individual, group, blanket, or franchise insurance policy or insurance agreement, a group hospital service contract, or an individual or group evidence of coverage that is offered by:(i)  an insurance company;(ii)  a group hospital service corporation operating under Chapter 842;(iii)  a fraternal benefit society operating under Chapter 885;(iv)  a Lloyd's plan operating under Chapter 941;(v)  a stipulated premium company operating under Chapter 884;  or(vi)  a health maintenance organization operating under Chapter 843;  and(B)  to the extent permitted by the Employee Retirement Income Security Act of 1974 (29 U.S.C. Section 1001 et seq.), a health benefit plan that is offered by:(i)  a multiple employer welfare arrangement as defined by Section 3 of that Act;  or(ii)  another analogous benefit arrangement;(2)  is offered by an approved nonprofit health corporation operating under Chapter 844;  or(3)  provides health and accident coverage through a risk pool created under Chapter 172, Local Government Code, notwithstanding Section 172.014, Local Government Code, or any other law.
    Added by Acts 2003, 78th Leg., ch. 1274, Sec. 3, eff. April 1, 2005.

    Sec. 1363.002.  EXCEPTION.  This chapter does not apply to:(1)  a plan that provides coverage:(A)  only for a specified disease or other limited benefit;(B)  only for accidental death or dismemberment;(C)  for wages or payments in lieu of wages for a period during which an employee is absent from work because of sickness or injury;(D)  as a supplement to a liability insurance policy;  or(E)  only for indemnity for hospital confinement;(2)  a small employer health benefit plan written under Chapter 1501;(3)  a Medicare supplemental policy as defined by Section 1882(g)(1), Social Security Act (42 U.S.C. Section 1395ss), as amended;(4)  a workers' compensation insurance policy;(5)  medical payment insurance coverage provided under a motor vehicle insurance policy;  or(6)  a long-term care policy, including a nursing home fixed indemnity policy, unless the commissioner determines that the policy provides benefit coverage so comprehensive that the policy is a health benefit plan as described by Section 1363.001.
    Added by Acts 2003, 78th Leg., ch. 1274, Sec. 3, eff. April 1, 2005.

    Sec. 1363.003.  MINIMUM COVERAGE REQUIRED.  (a)  A health benefit plan that provides coverage for screening medical procedures must provide to each individual enrolled in the plan who is 50 years of age or older and at normal risk for developing colon cancer coverage for expenses incurred in conducting a medically recognized screening examination for the detection of colorectal cancer.(b)  The minimum coverage required under this section must include:(1)  a fecal occult blood test performed annually and a flexible sigmoidoscopy performed every five years;  or(2)  a colonoscopy performed every 10 years.
    Added by Acts 2003, 78th Leg., ch. 1274, Sec. 3, eff. April 1, 2005.

    Sec. 1363.004.  NOTICE OF COVERAGE.  (a)  A health benefit plan issuer shall provide to each individual enrolled in the plan written notice of the coverage required under this chapter.(b)  The notice must be provided in accordance with rules adopted by the commissioner.
    Added by Acts 2003, 78th Leg., ch. 1274, Sec. 3, eff. April 1, 2005.

    Sec. 1363.005.  RULES.  The commissioner shall adopt rules as necessary to administer this chapter.
    Added by Acts 2003, 78th Leg., ch. 1274, Sec. 3, eff. April 1, 2005.


                    
