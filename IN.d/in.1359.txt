      
                        
    INSURANCE CODE
    TITLE 8. HEALTH INSURANCE AND OTHER HEALTH COVERAGES
    SUBTITLE E. BENEFITS PAYABLE UNDER HEALTH COVERAGES
    CHAPTER 1359. FORMULAS FOR INDIVIDUALS WITH PHENYLKETONURIA OR OTHER HERITABLE DISEASES
    Sec. 1359.001.  DEFINITIONS.  In this chapter:(1)  "Heritable disease" means an inherited disease that may result in mental or physical retardation or death.(2)  "Phenylketonuria" means an inherited condition that, if not treated, may cause severe mental retardation.
    Added by Acts 2003, 78th Leg., ch. 1274, Sec. 3, eff. April 1, 2005.

    Sec. 1359.002.  APPLICABILITY OF CHAPTER.  This chapter applies only to a group health benefit plan that is a group policy, contract, or certificate of health insurance or an evidence of coverage delivered, issued for delivery, or renewed in this state by:(1)  an insurance company;(2)  a group hospital service corporation operating under Chapter 842;  or(3)  a health maintenance organization operating under Chapter 843.
    Added by Acts 2003, 78th Leg., ch. 1274, Sec. 3, eff. April 1, 2005.

    Sec. 1359.003.  COVERAGE REQUIRED.  (a)  A group health benefit plan must provide coverage for formulas necessary to treat phenylketonuria or a heritable disease.(b)  The group health benefit plan must provide the coverage to the same extent that the plan provides coverage for drugs that are available only on the orders of a physician.
    Added by Acts 2003, 78th Leg., ch. 1274, Sec. 3, eff. April 1, 2005.


                    
