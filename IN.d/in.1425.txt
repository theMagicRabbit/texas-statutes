     
                        
    INSURANCE CODE
    TITLE 8. HEALTH INSURANCE AND OTHER HEALTH COVERAGES
    SUBTITLE E. BENEFITS PAYABLE UNDER HEALTH COVERAGES
    CHAPTER 1425.  APPLICATION OF SUBTITLE TO CERTAIN COVERAGE
    Sec. 1425.001.  EXEMPTION FROM APPLICATION OF SUBTITLE.  (a)  Except as provided by Section 1425.002, a provision of this subtitle that becomes effective on or after January 2, 2010, does not apply to:(1)  a plan that provides coverage only:(A)  for a specified disease or diseases or under an individual limited benefit policy;(B)  for accidental death or dismemberment;(C)  as a supplement to a liability insurance policy; or(D)  for dental or vision care;(2)  disability income insurance coverage or a combination of accident-only and disability income insurance coverage;(3)  credit insurance coverage;(4)  a hospital confinement indemnity policy;(5)  a Medicare supplemental policy as defined by Section 1882(g)(1), Social Security Act (42 U.S.C. Section 1395ss);(6)  a workers' compensation insurance policy;(7)  medical payment insurance coverage provided under a motor vehicle insurance policy;(8)  a long-term care insurance policy, including a nursing home fixed indemnity policy, except as provided by Subsection (b); or(9)  an occupational accident policy.(b)  A long-term care insurance policy, including a nursing home fixed indemnity policy, is subject to this subtitle if the commissioner determines that the policy provides benefits so comprehensive that it is a health benefit plan and should not be subject to the exemption provided under this section.
    Added by Acts 2009, 81st Leg., R.S., Ch. 228 (S.B. 1479), Sec. 1, eff. May 27, 2009.

    Sec. 1425.002.  SPECIFIC LANGUAGE CONTROLS.  A provision of this subtitle that becomes effective on or after January 2, 2010, and that requires coverage or the offer of coverage of a health care service or benefit applies to a plan or policy described by Section 1425.001(a) only to the extent expressly and specifically provided by law.
    Added by Acts 2009, 81st Leg., R.S., Ch. 228 (S.B. 1479), Sec. 1, eff. May 27, 2009.


                    
